% MIT No Attribution License (https://opensource.org/license/mit-0)
% 
% Copyright (c) 2019 Joonas T. Holmi (jtholmi@gmail.com), 2019-2021 Aalto University
% 
%-------------------------------------------------------------------------%
% This is CLEVER-statistics version of MRLCM-algorithm, described below.
%-------------------------------------------------------------------------%

%-------------------------------------------------------------------------%
% MRLCM-algorithm (or its data-transformed MDLCA-algorithm) tries to fix
% horizontal scanline errors. MRLCM (or MDLCA) corrects scanline errors,
% which are MULTIPLICATIVE (or ADDITIVE) in its physical nature like in
% Raman peak intensity data (or Raman peak position or AFM height data)!
%-------------------------------------------------------------------------%

% First version of MRLCM-algorithm was presented in Master Thesis
% (pp. 27–28, 35), J. T. Holmi (2016) "Determining the number of graphene
% layers by Raman-based Si-peak analysis", freely available to download at:
% http://urn.fi/URN:NBN:fi:aalto-201605122027
% The automated mask generation in this algorithm (and its data-transformed
% version) heavily rely on the code in clever_statistics_and_outliers.m.
function [out_2D, correction_2D, mask_2D] = apply_CMRLCM(in_2D, dim, mask_2D),
    % CLEVER Median Ratio Line Correction by Multiplication. This
    % MULTIPLICATIVE method preserves RATIOS (but does NOT preserve
    % DIFFERENCES)! In order to preserve DIFFERENCES, use ADDITIVE method
    % (CMDLCA) instead!
    
    % Median is used because it is one of the most outlier resistant
    % statistic with breakdown point of 50% dataset contamination. Also,
    % it sees true median behind multiplicative and additive constants,
    % because median(B) = median(c*A+d) = c*median(A)+d.
    
    % Updated 12.3.2019 by Joonas T. Holmi
    
    % Use the fact that CMRLCM = CMDLCA for log-transformed data!
    % log(rX) = log(Xa./Xb) = log(Xa)-log(Xb) = Ya-Yb = dY
    % log(rX) = dY OR rX = exp(dY) AND log(X) = Y OR X = exp(Y)
    in_2D = double(in_2D); % Required for boolean and integer input
    in_2D = log(in_2D); % Complex input are excluded by apply_MDLCA!
    if nargin < 3, [out_2D, correction_2D, mask_2D] = WITio.fun.image.apply_CMDLCA(in_2D, dim);
    else, [out_2D, correction_2D, mask_2D] = WITio.fun.image.apply_CMDLCA(in_2D, dim, mask_2D); end
    % Then restore linear-transformed data!
    out_2D = real(exp(out_2D)); % And ensure that imaginary-part is removed!
    correction_2D = real(exp(correction_2D)); % And ensure that imaginary-part is removed!
end
