% MIT No Attribution License (https://opensource.org/license/mit-0)
% 
% Copyright (c) 2019 Joonas T. Holmi (jtholmi@gmail.com), 2019-2021 Aalto University

% Mimics nansum in order to remove dependency on Statistics and Machine Learning Toolbox.
function y = mynansum(x, varargin),
    x(isnan(x)) = 0; % Set NaNs to zero
    y = sum(x, varargin{:});
end
