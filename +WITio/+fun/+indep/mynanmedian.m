% MIT No Attribution License (https://opensource.org/license/mit-0)
% 
% Copyright (c) 2019 Joonas T. Holmi (jtholmi@gmail.com), 2019-2021 Aalto University

% Used by WITio.fun.image.apply_MDLCA
function Y = mynanmedian(X, dim),
    % Mimics built-in nanmedian, which requires Statistics and Machine
    % Learning Toolbox. Input values are converted to double-type.
    Y = WITio.fun.indep.myquantile(X, 0.5, dim);
end
