% MIT No Attribution License (https://opensource.org/license/mit-0)
% 
% Copyright (c) 2019 Joonas T. Holmi (jtholmi@gmail.com), 2019-2021 Aalto University

% Inverse of java_objects_from_varargin.
function varargout = java_objects_to_varargout(jObjects),
    % A java.lang.Object array to varargout
    for ii = numel(jObjects):-1:1,
        varargout{ii} = jObjects(ii); % Benefit from Java's unboxing feature!
    end
end
