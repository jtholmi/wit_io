% MIT No Attribution License (https://opensource.org/license/mit-0)
% 
% Copyright (c) 2019 Joonas T. Holmi (jtholmi@gmail.com), 2019-2021 Aalto University

% Used by @wid\plot.m and WITio.fun.visual.clever_nanimagesc
function h = nanimagesc(varargin),
    % Sets nan values as transparent. Otherwise equivalent to imagesc.
    h = imagesc(varargin{:});
    data = get(h, 'CData');
    set(h, 'AlphaData', ~isnan(data)); % NaN = transparent
end
