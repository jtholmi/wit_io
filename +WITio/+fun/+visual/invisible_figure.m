% MIT No Attribution License (https://opensource.org/license/mit-0)
% 
% Copyright (c) 2019 Joonas T. Holmi (jtholmi@gmail.com), 2019-2021 Aalto University

% Used by @wid\plot.m and @wip\manager.m
function h = invisible_figure(varargin),
    % Equivalent to figure-method but creates it as invisible!
    DefaultFigureVisible = get(0, 'DefaultFigureVisible'); % Store visibility
    set(0, 'DefaultFigureVisible', 'off'); % Set invisibility
    h = figure(varargin{:});
    set(0, 'DefaultFigureVisible', DefaultFigureVisible); % Restore visibility
end
