% MIT No Attribution License (https://opensource.org/license/mit-0)
% 
% Copyright (c) 2019 Joonas T. Holmi (jtholmi@gmail.com), 2019-2021 Aalto University

% Used by +WITio\+tbx\ifnodesktop.m and +WITio\+tbx\+ui\sidebar_export.m
function varargout = export_fig(varargin),
    % Prefer WITio's export_fig
    WITio_export_fig = fullfile(WITio.tbx.path, 'third party', 'export_fig');
    old_path = addpath(genpath(WITio_export_fig), '-begin');
    ocu = onCleanup(@() path(old_path)); % Restore old path on exit
    
    % Suppress 'Warning: In a future release, UI components will not be
    % included in the output. To include UI components, use the exportapp function.'
    old_state = warning('off', 'MATLAB:print:ExcludesUIInFutureRelease');
    ocu2 = onCleanup(@() warning(old_state)); % Restore old warning on exit

    % Stop export_fig from promoting consulting services!
    clear export_fig;
    setpref('export_fig', 'promo_time', nan);

    % Stop export_fig from promoting updates (unless -nosilent overrides)!
    varargin{end+1} = '-silent';
    B_char = cellfun(@ischar, varargin);
    B_nosilent_char = strcmp(varargin(B_char), '-nosilent');
    if any(B_nosilent_char), % When -nosilent, remove all -(no)silent
        B_silent_char = strcmp(varargin(B_char), '-silent');
        B_keep = true(size(varargin));
        ind_char = find(B_char);
        B_keep(ind_char(B_nosilent_char)) = false;
        B_keep(ind_char(B_silent_char)) = false;
        varargin = varargin(B_keep);
    end

    % Export figures
    [varargout{1:nargout}] = export_fig(varargin{:});
end
