% MIT No Attribution License (https://opensource.org/license/mit-0)
% 
% Copyright (c) 2019 Joonas T. Holmi (jtholmi@gmail.com), 2019-2021 Aalto University

function h = clever_nanimagesc(varargin),
    h = WITio.fun.visual.nanimagesc(varargin{:});
    data = get(h, 'CData');
    
    % Clever caxis
    [~, ~, ~, ~, ~, cmin, cmax] = WITio.fun.clever_statistics_and_outliers(data(~isinf(data)), [], 4);
    if ~isnan(cmin) && ~isnan(cmax) && cmin ~= cmax,
        caxis([cmin cmax]);
    end
end
