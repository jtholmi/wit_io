% MIT No Attribution License (https://opensource.org/license/mit-0)
% 
% Copyright (c) 2019 Joonas T. Holmi (jtholmi@gmail.com), 2019-2021 Aalto University
% 
function varargout = dim_size_consistent_repmat(varargin),
    % Function repmats all inputs so that they all are equivalent in size.
    % First it tests if input sizes are consistent with each other.
    % For each input, each dimension is consistent if 1) it is singleton
    % OR 2) all nonsingleton input dimension sizes are equivalent.
    % If inconsistency is found, then function returns error.
    
    %% EXAMPLE 1: Equivalent case with meshgrid
    % Inputs with sizes [1 10], [70 1] produce outputs with size [70 10].
    % x = 1:10; % size(x) == [1 10]
    % y = 1:70; % size(y) == [1 70]
    % [X1, Y1] = meshgrid(x, y); % size(X1) == [10 70], size(Y1) == [10 70]
    % [X2, Y2] = WITio.fun.dim_size_consistent_repmat(x, y'); % Notice the transpose!
    % Here X1, Y1 are equivalent to X2, Y2!
    
    %% EXAMPLE 2: Complex case
    % Inputs with sizes [500 10 1 4], [1 10], [1 1], [1 10 3], [1 10 1 4]
    % produce outputs with size [500 10 3 4].
    % a = randn([500 10 1 4]);
    % b = randn([1 10]);
    % c = randn([1 1]);
    % d = randn([1 10 3]);
    % e = randn([1 10 1 4]);
    % [A, B, C, D, E] = WITio.fun.dim_size_consistent_repmat(a, b, c, d, e);
    % size(A) % [500 10 3 4]
    
    %% CODE
    if nargin > 0, % If inputs given
        % Get input dimension counts
        NDIMS_in = zeros(nargin, 1);
        for ii = 1:nargin, NDIMS_in(ii) = ndims(varargin{ii}); end
        ndims_max = max(NDIMS_in); % Find maximum dimension count

        % Get input sizes (with 1-padding)
        SIZE_in = ones(nargin, ndims_max);
        for ii = 1:nargin, SIZE_in(ii,1:NDIMS_in(ii)) = size(varargin{ii}); end

        % Find singletons for consistency test and repmat
        is_singleton = SIZE_in == 1;

        % Output size
        size_out = max(SIZE_in, [], 1);
        SIZE_out = repmat(size_out, [nargin 1]); % Repmat to simplify code

        % Test dimension size consistency
        is_out_consistent = SIZE_in == SIZE_out; % If consistent with output size
        is_consistent = (~is_singleton & is_out_consistent) | is_singleton;
        if any(~is_consistent(:)), error('Inconsistent dimension sizes!'); end

        % Repmat singleton dimensions to reach output size
        repmat_out = ones(nargin, ndims_max);
        repmat_out(is_singleton) = SIZE_out(is_singleton); % Replace singletons

        % Create dimension size consistent output
        for ii = nargin:-1:1, varargout{ii} = repmat(varargin{ii}, repmat_out(ii,:)); end
    end
end
