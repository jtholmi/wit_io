% MIT No Attribution License (https://opensource.org/license/mit-0)
% 
% Copyright (c) 2019 Joonas T. Holmi (jtholmi@gmail.com), 2019-2021 Aalto University

% Tests whether the code is ran under Octave or not.
function tf = is_octave(),
    persistent is_octave;
    if isempty(is_octave), is_octave = exist('OCTAVE_VERSION', 'builtin') == 5; end
    tf = is_octave;
end
