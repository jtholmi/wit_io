% MIT No Attribution License (https://opensource.org/license/mit-0)
% 
% Copyright (c) 2019 Joonas T. Holmi (jtholmi@gmail.com), 2019-2021 Aalto University

function [P, R2, SSres, Y_fit, R2_total, SSres_total] = fit_lineshape_gaussian(x, Y, P0, dim, varargin),
    % Gaussian lineshape fitting
    % P(1,:) = Intensity, P(2,:) = Pos, P(3,:) = Fwhm, P(4,:) = Offset
    [P, R2, SSres, Y_fit, R2_total, SSres_total] = WITio.fun.fit.fit_lineshape_arbitrary(@WITio.fun.fit.fun_lineshape_gaussian, x, Y, P0, dim, varargin{:});
end
