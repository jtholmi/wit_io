% MIT No Attribution License (https://opensource.org/license/mit-0)
% 
% Copyright (c) 2019 Joonas T. Holmi (jtholmi@gmail.com), 2019-2021 Aalto University
% 
% This can be used to validate the analytical solutions of the lineshape's
% Jacobian and Hessian matrices (if they exist). Although this requires
% Symbolic Math Toolbox, it is not programmatically used by WITio.
function [fun_Jf, fun_Hf, exception] = generate_Jacobian_and_Hessian_funs(fun_f, N_constants, N_steps),
    % Generates Jacobian and Hessian (if possible).
    % If not possible, return empty Jf and Hf, and generate exception.
    
    % Default number of simplification steps
    if nargin < 3, N_steps = 1; end
    
    % Assume all parameters are variables
    if nargin < 2, N_constants = 0; end
    
    % If no given input, Lorentzian function is set as an example.
    if nargin < 1,
        fun_f = @(X, A, B, C, D) A ./ (1 + (2.*(X-B)./C).^2) + D;
        N_constants = 1; % Consider 'X' as a constant.
    end
    
    exception = []; % Return no exception by default
    try
        % Get function parameters
        str_fun_f = func2str(fun_f);
        str_fun_params = regexp(str_fun_f, '@\(([^\)]*)\)', 'tokens', 'once');
        str_fun_params_split = regexp(str_fun_params{1}, '\s*,\s*', 'split');
        str_fun_vars_split = str_fun_params_split((N_constants+1):end);
        N_variables = numel(str_fun_vars_split);
        
        % Generate symbolic variable matrix
        X = sym('v', [1 N_variables]);
        for ii = 1:N_variables,
            X(ii) = sym(str_fun_vars_split{ii});
        end
        
        % Convert function handle to symbolic function
        f = sym(fun_f);
        f = simplify(f, N_steps);

        % Generate symbolic Jacobian matrix
        Jf = sym('Jf', [1 N_variables]);
        for ii = 1:N_variables,
            Jf(ii) = diff(f, X(ii));
            Jf(ii) = simplify(Jf(ii), N_steps);
        end

        % Generate symbolic Hessian matrix
        Hf = sym('Hf', [N_variables N_variables]);
%         kk = 1;
        for ii = 1:N_variables,
            for jj = 1:N_variables,
                Hf(ii,jj) = diff(Jf(ii), X(jj)); % dJf_ii/dX_jj = d(df/dX_ii)/dX_jj = d^2f/dX_jjdX_ii
                Hf(ii,jj) = simplify(Hf(ii,jj), N_steps);
%                 Hf(ii,jj) = kk;
%                 kk = kk + 1;
            end
        end

        % Convert symbolic function matrices to function handle matrices
        fun_Jf = matlabFunction(Jf, 'Vars', str_fun_params_split); % Keep original parameters
        fun_Hf = matlabFunction(Hf, 'Vars', str_fun_params_split); % Keep original parameters
    catch exception
        [fun_Jf, fun_Hf] = deal([]); % Generation unsuccessful!
    end
end
