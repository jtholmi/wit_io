% MIT No Attribution License (https://opensource.org/license/mit-0)
% 
% Copyright (c) 2019 Joonas T. Holmi (jtholmi@gmail.com), 2019-2021 Aalto University
% 
% Used by WITio.fun.fit.fit_lineshape_automatic_guess
function [line_length, line_label] = bw2lines(bw_lines),
    % Function converts the 2nd dimension of a boolean map to lines.
    % Returns maps for length and label of lines.
    
    % Simulate that of bw_lines_cumsum_padded = padarray(bw_lines_cumsum, [0 1], 0, 'pre'):
    bw_lines_cumsum = cumsum(bw_lines, 2);
    bw_lines_cumsum_padded = zeros(size(bw_lines_cumsum, 1), size(bw_lines_cumsum, 2)+1);
    bw_lines_cumsum_padded(:,2:end) = bw_lines_cumsum;
    
    % Simulate that of bw_lines_padded = padarray(bw_lines, [0 1], 0, 'both'):
    bw_lines_padded = zeros(size(bw_lines, 1), size(bw_lines, 2)+2);
    bw_lines_padded(:,2:end-1) = bw_lines;
    
    % Preparation
    line_cumsum = bw_lines_cumsum_padded'; % Cumsum when on line
    line_ends = diff(bw_lines_padded, [], 2)'; % Locate lines
    is_line_right_end = line_ends < 0; % -1 for right end
    is_line_left_end = line_ends > 0; % +1 for left end
    
    % Line lengths
    line_with_cumsum_reset = zeros(size(line_cumsum)); % Add line length to left side and its negative to outside right side
    line_with_cumsum_reset(is_line_right_end) = line_cumsum(is_line_right_end)-line_cumsum(is_line_left_end);
    line_with_cumsum_reset(is_line_left_end) = line_cumsum(is_line_left_end)-line_cumsum(is_line_right_end);
%     line_length = cumsum(line_with_cumsum_reset, 1, 'reverse')'; % Cumsum with 'reverse' does not work in 2012b! % Transpose 
    line_length = flipud(cumsum(flipud(line_with_cumsum_reset), 1))'; % Operate cumsum like 'reverse' % Transpose
    
    % Restore the original shape
    line_length = line_length(:,2:end);
    
    if nargout > 1,
        % Line labels
        label_with_cumsum_reset = zeros(size(line_cumsum));
        label_number = reshape(cumsum(is_line_right_end(:)), size(line_cumsum));
        label_number(~is_line_right_end) = 0;
        label_with_cumsum_reset(is_line_right_end) = label_number(is_line_right_end);
        label_with_cumsum_reset(is_line_left_end) = -label_number(is_line_right_end);
%         line_label = cumsum(label_with_cumsum_reset, 1, 'reverse')'; % Cumsum with 'reverse' does not work in 2012b! % Transpose 
        line_label = flipud(cumsum(flipud(label_with_cumsum_reset), 1))'; % Operate cumsum like 'reverse' % Transpose

        % Restore the original shape
        line_label = line_label(:,2:end);
    end
end
