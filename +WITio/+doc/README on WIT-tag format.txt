===========================================================================
%% Formatting of WIP/WID-files for versions 0-7. Listing is NOT exhaustive!
%% This represents a WIT tree structure consisting of many WIT-branches.
%% v0-v5 = legacy WITec software versions, i.e. WITec Control 1.60.3.3 and Project 2.10.3.3
%% v6 = i.e. WITec Project FOUR 4.1.12
%% v7 = i.e. WITec Suite (Control + Project) FIVE 5.1.8.64 and SIX 6.1.6.130

v0	Only one file analyzed.				(Any file donations are welcomed.)
v1	NO FILE ANALYSIS DONE.				(Any file donations are welcomed.)
v2	Preliminary file analysis done.
v3	NO FILE ANALYSIS DONE.				(Any file donations are welcomed.)
v4	NO FILE ANALYSIS DONE.				(Any file donations are welcomed.)
v5	Thorough file analysis done.
v6	Thorough file analysis done.
v7	Thorough file analysis done.
v8-	Not known whether v8- exists yet.	(Any file donations are welcomed.)

Please send the v0-v1, v3-v4, v8- file donations to jtholmi@gmail.com.

More comments on v0 (20.2.2020):
File formats of only TDBitmap, TDImage, TDText, TDZInterpretation,
TDSpaceInterpretation and TDSpaceTransformation were available. Some of the
details have been extrapolated.

KEY ASSUMPTIONS FOR THE CODE IMPLEMENTATION:
(1) File format content increase only monotonically for legacy versions v0-v5.
(2) WITec software can handle (or ignore) any unused and unrelated WIT-tags.
(3) WITec software cannot handle absence of the essential WIT-tags.
(4) HENCE (from (1)-(3)), legacy WITec software (for v0-v5) can read v5 files.
===========================================================================

***************************************************************************
%% Last updated 10.8.2024 by Joonas T. Holmi
***************************************************************************

MAGIC string (8x1 uint8=>char) in the beginning of the WIP/WID-files: % (a row vector in MATLAB)
	= 'WIT_PRCT'/'WIT_DATA' (v0-v5)
	= 'WIT_PR06'/'WIT_DA06' (v6-v7)

===========================================================================
%% NESTED WIT-TAG FILE FORMAT (after the 8-byte MAGIC string above)
===========================================================================

CONSTRUCT the (first) wit-class object and fill-in its key properties. File
binary is read further at each line by the byte size of the class specified
in ()-brackets. The file format is always stored in LITTLE-ENDIAN ORDERING.

NameLength (uint32)    % Determines how many bytes to read for Name
                       % Define N = NameLength
Name (Nx1 uint8=>char) % Text always 'windows-1252'-encoded? (a row vector in MATLAB)
Type (uint32) = 0-9    % 0=wit, 1=unused, 2=double, 3=single, 4=int64, 5=int32, 6=datetime, 7=uint8, 8=logical, 9=string
                       % Here Type == 1 is probably always unused based on the data so far
Start (uint64)         % File pointer to the start of Data
End (uint64)           % File pointer to the end of Data, pointing 1-byte beyond Data
                       % Define M = End-Start
Data (Mx1 uint8=>Type) % An array of Type-class, which few special cases:
                       % - If Type == 0: RECURSIVELY CONSTRUCT a nested wit Tree object array
                       % - If Type == 6: A datetime array, where each datetime is constructed from an uint16 array: Year, Month, Day, Hour, Minute, Second, Millisecond
                       % - If Type == 9: A string array, where each string is constructed from its StringLength (uint32) and String pair (like in the NameLength-Name pair case)
                       % Data is always written in LITTLE-ENDIAN ORDERING

REPEAT ABOVE TILL THE END OF FILE.

===========================================================================
%% MAIN STRUCTURE of WIP-files
===========================================================================

WITec Project (wit)
    Version (int32) = 0-7 (v0-v7)
    SystemInformation (wit) (not in v0,v2)
        LastApplicationSessionIDs (wit)
            ...
		ServiceID (char) (v6-v7)            % Text always 'windows-1252'-encoded?
		LicenseID (char) (v6-v7)            % Text always 'windows-1252'-encoded?
		SystemID (char) (v7)                % Text always 'windows-1252'-encoded?
        ApplicationVersions (wit)
            ...
    NextDataID (int32)
    ShellExtensionInfo (wit) (not in v0)
        ThumbnailPreviewBitmap (wit)
            SizeX (int32)
            SizeY (int32)
            BitsPerPixel (int32)
            BitmapData (uint8)
    Data (wit)
        DataClassName 0 (char)              % Text always 'windows-1252'-encoded?
        Data 0 (wit)
            TData (wit)
            ...
        ...
        NumberOfData (int32)
    Viewer (wit)
        ViewerClassName 0 (char)            % Text always 'windows-1252'-encoded?
        Viewer 0 (wit)
            ...
        ...
        NumberOfViewer (int32)

NOTE:
WITec software requires unique Data/DataClassName indices that begin from 0
and end with NumberOfData-1. Not obeying this will lead to missing data or
crash. The ordering does not matter, but will be automatically relabeled in
the ascending order by WITec software.



===========================================================================
%% MAIN STRUCTURE of WID-files
===========================================================================

WITec Data (wit)
    Version (int32) = 0-7 (v0-v7)
    SystemInformation (wit) (not in v0,v2)
        LastApplicationSessionIDs (wit)
            ...
		ServiceID (char) (v6-v7)            % Text always 'windows-1252'-encoded?
		LicenseID (char) (v6-v7)            % Text always 'windows-1252'-encoded?
		SystemID (char) (v7)                % Text always 'windows-1252'-encoded?
        ApplicationVersions (wit)
            ...
    Data (wit)
        DataClassName 0 (char)              % Text always 'windows-1252'-encoded?
        Data 0 (wit)
            TData (wit)
            ...
        ...
        NumberOfData (int32)



***************************************************************************
%% DATA-SEGMENT
***************************************************************************

===========================================================================
%% Each "Data <integer>"-tag includes a TData-tag (TO BE EXCLUDED LATER!):
===========================================================================

TData (wit)
	Version (int32) = 0
	ID (int32)
	ImageIndex (int32)
	Caption (char)                          % Text always 'windows-1252'-encoded?
	MetaData (wit) (v7)
		...
	HistoryList (wit)
		Number Of History Entries (int32)
		Dates (uint16)                      % Datetime array, see Type == 6 above
		Histories (char)                    % Text always 'windows-1252'-encoded?
		Types (int32)



===========================================================================
%% Content of "Data <integer>"-tag with ClassName of "TDBitmap":
===========================================================================

TDStream (wit) (v0-v5)
	Version (int32) = 0 (v0-v5)
	StreamSize (int32)
	StreamData (uint8)                      % BMP-formatted file
TDBitmap (wit)
	Version (int32) = 0 (v0-v5), = 1 (v6-v7)
	SizeX (int32) (v6-v7)
	SizeY (int32) (v6-v7)
	SpaceTransformationID (int32)
	SecondaryTransformationID (int32) (v7)
	BitmapData (wit) (v6-v7)
		Dimension (int32)
		DataType (int32) = 2 (v6-v7)        % Due to 2, Data's type is int32
		Ranges (int32)
		Data (determined by DataType)       % RBGA but alpha-channel always unused?



===========================================================================
%% Content of "Data <integer>"-tag with ClassName of "TDGraph":
===========================================================================

TDGraph (wit)
	Version (int32) = 0 (v0-v6), = 1 (v7)
	SizeX (int32)
	SizeY (int32)
	SizeGraph (int32)
	SpaceTransformationID (int32)
	SecondaryTransformationID (int32) (v7)
	XTransformationID (int32)
	XInterpretationID (int32)
	ZInterpretationID (int32)
	DataFieldInverted (logical) (v7)
	GraphData (wit)
		Dimension (int32)
		DataType (int32)
		Ranges (int32)
		Data (determined by DataType)
	LineChanged (logical)
	LineValid (logical)



===========================================================================
%% Content of "Data <integer>"-tag with ClassName of "TDImage":
===========================================================================

TDImage (wit)
	Version (int32) = 0 (v0-v5), = 1 (v6-v7)
	SizeX (int32)
	SizeY (int32)
	PositionTransformationID (int32)
	SecondaryTransformationID (int32) (v7)
	ZInterpretationID (int32)
	Average (double)
	Deviation (double)
	LineAverage (double)
	LineSumSqr (double)
	LineSum (double)
	LineA (double)
	LineB (double)
	LineChanged (logical)
	LineValid (logical)
	ImageDataIsInverted (logical) (v6-v7)
	ImageData (wit)
		Dimension (int32)
		DataType (int32)
		Ranges (int32)
		Data (determined by DataType)



===========================================================================
%% Content of "Data <integer>"-tag with ClassName of "TDText":
===========================================================================

TDStream (wit)
	Version (int32) = 0
	StreamSize (int32)
	StreamData (uint8)                      % RTF-v1-formatted file with 'windows-1252'-encoding
TDText (wit) (v7 Suite SIX)
	Version (int32) = 1 (v7 Suite SIX)
	IsUserText (logical) (v7 Suite SIX)



***************************************************************************
%% INTERPRETATIONS
***************************************************************************

if TDFrequencyInterpretation:               % Frequency (1-dimensional point)
UnitIndex   StandardUnit            Description
0           µHz                     Microhertz
1           mHz                     Millihertz
2           Hz                      Hertz
3           kHz                     Kilohertz
4           MHz                     Megahertz
5           GHz                     Gigahertz
6           THz                     Terahertz
>6          a.u. (Hz internally)    Arbitrary Unit

if TDInverseSpaceInterpretation:            % Px, Py, Pz (3-dimensional point)
UnitIndex   StandardUnit            Description
0           1/m                     Reciprocal Meters
1           1/mm                    Reciprocal Millimeters
2           1/µm                    Reciprocal Micrometers
3           1/nm                    Reciprocal Nanometers
4           1/Å                     Reciprocal Ångströms
5           1/pm                    Reciprocal Picometers
>5          a.u. (1/µm internally)  Arbitrary Unit

if TDPhaseInterpretation:                   % Phase (1-dimensional point)
UnitIndex   StandardUnit            Description
0           rad                     Radians
1           mrad                    Milliradians
2           °                       Degrees
3           grad                    Gradians
4           mgrad                   Milligradians
>4          a.u. (rad internally)   Arbitrary Unit

if TDSpaceInterpretation:                   % X, Y, Z (3-dimensional point)
UnitIndex   StandardUnit            Description
0           m                       Meters
1           mm                      Millimeters
2           µm                      Micrometers
3           nm                      Nanometers
4           Å                       Ångströms
5           pm                      Picometers
>5          a.u. (µm internally)    Arbitrary Unit

if TDSpectralInterpretation:                % Spectral Position (1-dimensional)
UnitIndex   StandardUnit            Description
0           nm                      Nanometers
1           µm                      Micrometers
2           1/cm                    Spectroscopic Wavenumber
3           rel. 1/cm               Raman Shift
4           eV                      Energy
5           meV                     Energy
6           rel. eV                 Relative Energy
7           rel. meV                Relative Energy
>7          a.u. (nm internally)    Arbitrary Unit

if TDTimeInterpretation:                    % Time (1-dimensional point)
UnitIndex   StandardUnit            Description
0           h                       Hours
1           min                     Minutes
2           s                       Seconds
3           ms                      Milliseconds
4           µs                      Microseconds
5           ns                      Nanoseconds
6           ps                      Picoseconds
7           fs                      Femtoseconds
>7          a.u. (s internally)     Arbitrary Unit

if TDZInterpretation:                       % ZValue (1-dimensional point)
UnitIndex   StandardUnit            Description
0           UnitName                UnitName<TDZInterpretation<TDZInterpretation
>0          a.u. (a.u. internally)  Arbitrary Unit

===========================================================================
%% Content of "Data <integer>"-tag with ClassName of "TDFrequencyInterpretation",
"TDInverseSpaceInterpretation", "TDPhaseInterpretation", "TDSpaceInterpretation",
and "TDTimeInterpretation":
===========================================================================

TDInterpretation (wit)
	Version (int32) = 0
	UnitIndex (int32)



===========================================================================
%% Content of "Data <integer>"-tag with ClassName of "TDSpectralInterpretation":
===========================================================================

TDInterpretation (wit)
	Version (int32) = 0
	UnitIndex (int32)
TDSpectralInterpretation (wit)
	Version (int32) = 0
	ExcitationWaveLength (double)



===========================================================================
%% Content of "Data <integer>"-tag with ClassName of "TDZInterpretation":
===========================================================================

TDInterpretation (wit)
	Version (int32) = 0
	UnitIndex (int32)
TDZInterpretation (wit)
	Version (int32) = 0
	UnitName (char)                         % Text always 'windows-1252'-encoded?



***************************************************************************
%% TRANSFORMATIONS
***************************************************************************

UnitKind    StandardUnit    Interpretation  Transformation
0           ?               ?               ?
1           µm              Space           Space
2           nm              Spectral        Spectral    % Default transformation?
3           s               Time            Linear      % Default transformation?
4           a.u.            Z (= Data-axis) Linear      % Default transformation?
5           Hz              Frequency       Linear      % Default transformation?
6           1/µm            InverseSpace    Space
7           rad             Phase           Linear      % Default transformation?

===========================================================================
%% Content of "Data <integer>"-tag with ClassName of "TDLinearTransformation":
===========================================================================

TDTransformation (wit)
	Version (int32) = 0
	StandardUnit (char)                     % See UnitKind above, Text always 'windows-1252'-encoded?
	UnitKind (int32)
	InterpretationID (int32) (not in v0,v2)
	IsCalibrated (logical) (not in v0,v2)
TDLinearTransformation (wit)
	Version (int32) = 0
	ModelOrigin_D (double) (not in v0)
	WorldOrigin_D (double) (not in v0)
	Scale_D (double) (not in v0)
	ModelOrigin (single)
	WorldOrigin (single)
	Scale (single)



===========================================================================
%% Content of "Data <integer>"-tag with ClassName of "TDLUTTransformation":
===========================================================================

TDTransformation (wit)
	Version (int32) = 0
	StandardUnit (char)                     % Text always 'windows-1252'-encoded?
	UnitKind (int32)
	InterpretationID (int32)
	IsCalibrated (logical)
TDLUTTransformation (wit) (not in v0,v2)
	Version (int32) = 0
	LUTSize (int32)
	LUT (double)
	LUTIsIncreasing (logical)
	



===========================================================================
%% Content of "Data <integer>"-tag with ClassName of "TDSpaceTransformation":
===========================================================================

TDTransformation (wit)
	Version (int32) = 0
	StandardUnit (char)                     % Text always 'windows-1252'-encoded?
	UnitKind (int32)
	InterpretationID (int32)
	IsCalibrated (logical)
TDSpaceTransformation (wit)
	Version (int32) = 0
	ViewPort3D (wit)
		ModelOrigin (1x3 double)
		WorldOrigin (1x3 double)
		Scale (3x3 double)
		Rotation (3x3 double)
	LineInformationValid (logical)
	LineStart_D (Nx3 double) (not in v0)    % 1st point == WorldOrigin<ViewPort3D
	LineStart (Nx3 single)                  % 1st point
	LineStop_D (Nx3 double) (not in v0)     % N+1'th point
	LineStop (Nx3 single)                   % N+1'th point
	NumberOfLinePoints (int32)              % N points

%% MORE INFORMATION:
TData
    ImageIndex = 0 (for Image, Line, Point), = 1 (for Cross-section)



===========================================================================
%% Content of "Data <integer>"-tag with ClassName of "TDSpectralTransformation":
===========================================================================

TDTransformation (wit)
	Version (int32) = 0
	StandardUnit (char)                     % Text always 'windows-1252'-encoded?
	UnitKind (int32)
	InterpretationID (int32)
	IsCalibrated (logical)
TDSpectralTransformation (wit)
	Version (int32) = 0
	SpectralTransformationType (int32)
	Polynom (1x3 double)                    % Supports the 2nd order polynomials
	nC (double)
	LambdaC (double)
	Gamma (double)
	Delta (double)
	m (double)
	d (double)
	x (double)
	f (double)
	FreePolynomOrder (int32) (not in v0,v2)
	FreePolynomStartBin (double) (not in v0,v2)
	FreePolynomStopBin (double) (not in v0,v2)
	FreePolynom (double) (not in v0,v2)


