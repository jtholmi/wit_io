% MIT No Attribution License (https://opensource.org/license/mit-0)
% 
% Copyright (c) 2019 Joonas T. Holmi (jtholmi@gmail.com), 2019-2021 Aalto University
% 
% Class for handle listener event data
classdef handle_listener_event_data < event.EventData, % Since R2008a
    properties (SetAccess = private)
        fun;
        varargin;
    end
    methods 
        function obj = handle_listener_event_data(fun, varargin),
            obj.fun = fun;
            obj.varargin = varargin;
        end
    end
end
