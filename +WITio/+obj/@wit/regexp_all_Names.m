% MIT No Attribution License (https://opensource.org/license/mit-0)
% 
% Copyright (c) 2019 Joonas T. Holmi (jtholmi@gmail.com), 2019-2021 Aalto University

function tags = regexp_all_Names(obj, pattern), %#ok
    % Finds Tag(s) by specified Name-pattern.
    tags = obj(~cellfun(@isempty, regexp({obj.NameNow}, pattern, 'once'))); % Benefit from cell-array speed-up
    Descendants = [obj.ChildrenNow];
    while ~isempty(Descendants), %#ok
        tags = [tags Descendants(~cellfun(@isempty, regexp({Descendants.NameNow}, pattern, 'once')))]; %#ok % Benefit from cell-array speed-up];
        Descendants = [Descendants.ChildrenNow];
    end
end
