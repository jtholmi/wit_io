% MIT No Attribution License (https://opensource.org/license/mit-0)
% 
% Copyright (c) 2019 Joonas T. Holmi (jtholmi@gmail.com), 2019-2021 Aalto University

function tags = search(obj, varargin), %#ok
    % Finds Tag(s) by specified Name(s). The last given Name is the search
    % criteria for the given obj. The other Name(s) are recursively matched
    % for the obj descendants. This is faster than regexp because this uses
    % strcmp (or regexp if input was enclosed by curly {}-brackets) and
    % quits searching from unmatched branches.
    if isempty(obj) || isempty(varargin), tags = obj; return; end % Return obj if empty or no search criteria given
    if ~iscell(varargin{end}), match = strcmp({obj.NameNow}, varargin{end}); % Use strcmp
    else, match = ~cellfun(@isempty, regexp({obj.NameNow}, varargin{end}{1}, 'once')); end % Use regexp if enclosed in {}-brackets
    if ~any(match), tags = WITio.obj.wit.empty; return; end % Return empty if no matches
    if numel(varargin) == 1, tags = obj(match); % Return matches if no more criteria found
    else, tags = search([obj(match).ChildrenNow WITio.obj.wit.empty], varargin{1:end-1}); end % Continue search with the matched obj children
end
