% MIT No Attribution License (https://opensource.org/license/mit-0)
% 
% Copyright (c) 2019 Joonas T. Holmi (jtholmi@gmail.com), 2019-2021 Aalto University

% This function determines whether or not the file reading/writing code
% should swap data endianess. To author's best knowledge, all wit-Tree
% formatted files are always LITTLE-ENDIAN ORDERED. For this reason, the
% operating system's native endianess must be taken into account. 
function swapEndianess = swap_endianess(), %#ok
    toLittleEndian = true; % By default: Write as little endian
    % Decide if endianess should be swapped
    [~, ~, endian] = computer;
    if strcmp(endian, 'B'), %#ok % Computer uses BIG-ENDIAN ORDERING
        swapEndianess = toLittleEndian; % Swap if to write little endian
    else, %#ok % Otherwise ASSUME computer to use LITTLE-ENDIAN ORDERING
        swapEndianess = ~toLittleEndian; % Swap if to write big endian
    end
end
