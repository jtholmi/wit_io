% MIT No Attribution License (https://opensource.org/license/mit-0)
% 
% Copyright (c) 2019 Joonas T. Holmi (jtholmi@gmail.com), 2019-2021 Aalto University
% 
% Class for handle controller via weak references
classdef handle_listener < handle, % Since R2008a
    events
        listen_handle_event;
    end
    properties (SetAccess = private, Dependent) % READ-ONLY, DEPENDENT
        all;
    end
    properties (SetAccess = private, Hidden) % READ-ONLY
        temporary_objects; % This must always be cleared to avoid issues with MATLAB's garbace collector!
        empty_handle_listener = event.listener.empty; % To avoid repeated calls to empty!
    end
    %% PUBLIC METHODS
    methods
        % To keep only weak references to the objects array, each returned
        % event.listener must be only be stored in its corresponding object
        % of the objects array! Omit property-input to decide elsewhere
        % where to store the handle_listeners array content. If given, then
        % they will be stored to each object's property in the objects
        % array.
        function handle_listeners = add(obj, objects, property),
            setProperty = nargin > 2;
            handle_listeners = obj.empty_handle_listener;
            % Loop to create one event per object handle
            for ii = numel(objects):-1:1,
                fun = @(src, event_data) event_data.fun(objects(ii), event_data.varargin{:});
                handle_listeners(ii) = event.listener(obj, 'listen_handle_event', fun);
                if setProperty, objects(ii).(property) = handle_listeners(ii); end
            end
        end
        function objects = get.all(obj),
            objects = obj.listen_handle(@obj.fun_all);
        end
        function varargout = all_parsed(obj, parser, UniformOutput),
            if nargin < 3, UniformOutput = false; end % By default, assume non-uniform parser output
            obj.temporary_objects = obj.all;
            varargout = obj.postconditioner(parser, nargout, UniformOutput);
        end
        function objects = match_all(obj, tester),
            objects = obj.listen_handle(@obj.fun_match_all, tester);
        end
        function varargout = match_all_parsed(obj, tester, parser, UniformOutput),
            if nargin < 4, UniformOutput = false; end % By default, assume non-uniform parser output
            obj.temporary_objects = obj.match_all(tester);
            varargout = obj.postconditioner(parser, nargout, UniformOutput);
        end
        function object = match_any(obj, tester),
            object = obj.listen_handle(@obj.fun_match_any, tester);
        end
        function varargout = match_any_parsed(obj, tester, parser, UniformOutput),
            if nargin < 4, UniformOutput = false; end % By default, assume non-uniform parser output
            obj.temporary_objects = obj.match_any(tester);
            varargout = obj.postconditioner(parser, nargout, UniformOutput);
        end
    end
    %% PRIVATE METHODS
    methods (Access = private)
        function objects = listen_handle(obj, fun, varargin),
            ocu = onCleanup(@() obj.clear()); % Ensure safe clearing of temporary values on exit
            notify(obj, 'listen_handle_event', WITio.obj.handle_listener_event_data(fun, varargin{:}));
            % According to MATLAB's documentation, all listener callbacks
            % must be synchronously fired with notify-call [1].
            % [1] https://www.mathworks.com/help/matlab/matlab_oop/callback-execution.html
            objects = obj.temporary_objects;
        end
        function clear(obj),
            obj.temporary_objects = []; % Clearing any references to objects is crucial to allow MATLAB's garbage collector to work
        end
        function fun_all(obj, object),
            if isempty(obj.temporary_objects), obj.temporary_objects = object;
            else, obj.temporary_objects(end+1,1) = object; end
        end
        function fun_match_all(obj, object, tester),
            if tester(object),
                if isempty(obj.temporary_objects), obj.temporary_objects = object;
                else, obj.temporary_objects(end+1,1) = object; end
            end
        end
        function fun_match_any(obj, object, tester),
            if isempty(obj.temporary_objects) && tester(object),
                obj.temporary_objects = object;
            end
        end
        % Parse obj.temporary_objects into the requested number of outputs.
        % It returns a cell array (or a nested cell array for non-uniform
        % outputs) that can be directly passed to varargout in an external
        % call.
        function outputs = postconditioner(obj, parser, N_outputs, UniformOutput),
            ocu = onCleanup(@() obj.clear()); % Ensure safe clearing of temporary values on exit
            objects = obj.temporary_objects;
            N_objects = numel(objects);
            if N_objects == 0,
                if UniformOutput, outputs = {[]}; % Empty cell array
                else, outputs = {{}}; end % Empty nested cell array
                return;
            end
            % Collect all outputs for all objects
            temporary_outputs = cell(N_objects, N_outputs);
            for ii = 1:N_objects,
                [temporary_outputs{ii,1:N_outputs}] = parser(objects(ii));
            end
            % For uniform output, force all outputs to row vectors
            if UniformOutput,
                for ii = 1:numel(temporary_outputs),
                    temporary_outputs{ii} = reshape(temporary_outputs{ii}, 1, []);
                end
            end
            % Create final output
            outputs = cell(1, N_outputs);
            if UniformOutput, % For uniform output, construct a cell array
                for ii = 1:N_outputs,
                    outputs{ii} = cat(1, temporary_outputs{:,ii});
                end
            else, % For non-uniform output, construct a nested cell array
                for ii = 1:N_outputs,
                    outputs{ii} = temporary_outputs(:,ii);
                end
            end
        end
    end
end
