% MIT No Attribution License (https://opensource.org/license/mit-0)
% 
% Copyright (c) 2019 Joonas T. Holmi (jtholmi@gmail.com), 2019-2021 Aalto University

% THIS DOES NOT SUPPORT WIP PROJECT OBJECT ARRAYS!
function [ValueUnit, varargout] = transform_forced(obj, T, varargin),
    ValueUnit = WITio.obj.wip.ArbitraryUnit; % Default ValueUnit
    varargout = cellfun(@double, varargin, 'UniformOutput', false); % Default Value
    
    if isempty(T), return; end % Do nothing if empty Transformation
    T_Data = T.Data; % Load Data-structure only once (to save CPU time)

    % Interpret input
    [ValueUnit, varargout{1:nargout-1}] = WITio.obj.wip.transform(T, varargout{:});
    % Override units
    if ~isempty(obj),
        Interpretation = T_Data.TDTransformation.InterpretationID; % Prefer the found interpretation
        if isempty(Interpretation), % But if it is not found, then use the known type
            switch(T.Type),
                case 'TDLinearTransformation', % Do nothing
                case 'TDLUTTransformation', % Do nothing
                case 'TDSpaceTransformation',
                    if T_Data.TDTransformation.UnitKind == 1, % Space (X, Y, Z)
                        Interpretation = 'TDSpaceInterpretation';
                    elseif T_Data.TDTransformation.UnitKind == 6, % InverseSpace (Px, Py, Pz)
                        Interpretation = 'TDInverseSpaceInterpretation';
                    end
                case 'TDSpectralTransformation', Interpretation = 'TDSpectralInterpretation';
            end
        end
        [ValueUnit, varargout{1:nargout-1}] = obj.interpret_forced(Interpretation, ValueUnit, ValueUnit, varargout{:});
    end
end
