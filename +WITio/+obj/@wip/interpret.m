% MIT No Attribution License (https://opensource.org/license/mit-0)
% 
% Copyright (c) 2019 Joonas T. Holmi (jtholmi@gmail.com), 2019-2021 Aalto University

function [ValueUnit, varargout] = interpret(I, Unit_new, Unit_old, varargin),
    % * If Unit_old is empty, then default interpretation is assumed.
    % * Both Unit_new and Unit_old can either be UnitIndex (number input)
    % or ValueUnit (character input). Any mixture of those is accepted.
    % * Fields such as 'TDSpectralInterpretation' and 'TDZInterpretation'
    % will consume first extra input because of needed extra parameter.
    % * NOTE: ERRORS ON INVALID VALUES OF Unit_new or Unit_old!
    % MAY HAVE PERFORMANCE BOTTLENECKS (19.10.2017)
    if nargin < 2, Unit_new = []; end % Default Unit_new
    if nargin < 3, Unit_old = []; end % Default Unit_old
    
    PixelUnit = WITio.obj.wip.ArbitraryUnit; % Treated same way as the default units
    
    ValueUnit = []; % Default ValueUnit
    varargout = cellfun(@double, varargin, 'UniformOutput', false); % Default Value
    
    if ~isempty(Unit_old) && ischar(Unit_old), ValueUnit = Unit_old; end % If given Unit_old is ValueUnit_old
    
    if isempty(I), return; end % Do nothing if empty Interpretation
    
    if isa(I, 'WITio.obj.wid'),
        Type = I.Type; % Get its Type
        I = I.Data; % Get its Data-struct
    end
    if isstruct(I), % If Interpretation-struct
        if isempty(Unit_old), Unit_old = I.TDInterpretation.UnitIndex; end
        if isempty(Unit_new), Unit_new = I.TDInterpretation.UnitIndex; end
    elseif iscell(I) && numel(I) == 2, % If cell-array (where 1st == Type, 2nd == x0).
        Type = I{1};
    elseif ischar(I), % If string == Type
        Type = I;
        I = {Type, NaN}; % Create cell-array (where 1st == Type, 2nd == x0)
    else,
        warning('Invalid I! Should be wid, cell-array or string. SKIPPING...');
        return;
    end
    
    % Specified interpretations were reverse engineered to achieve interoperability (13.7.2016)
    Types = {'TDSpaceInterpretation', 'TDSpectralInterpretation', ...
        'TDTimeInterpretation', 'TDZInterpretation', ...
        'TDFrequencyInterpretation', 'TDInverseSpaceInterpretation', ...
        'TDPhaseInterpretation'};
    SubTypes = {'Space', 'Spectral', 'Time', 'Data', 'Frequency', 'InverseSpace', 'Phase'};
    DefaultStandardUnits = {'um', 'nm', 's', 'a.u.', 'Hz', '1/um', 'rad'};
    Letters = {'x', 'l', 't', 'z', 'f', '/x', 'o'};
    Type = strrep(Type, 'Z', 'Data');
    bw_find = strcmpi(Letters, strrep(Type, 'λ', 'l')) | ...
        strcmp(DefaultStandardUnits, strrep(Type, 'µ', 'u')) | ...
        strncmpi(SubTypes, Type, numel(Type)) | strcmpi(Types, strrep(Type, 'Data', 'Z'));
    if sum(bw_find) == 1, Type = Types{bw_find};
    elseif sum(bw_find) > 1, error('TWO OR MORE MATCHES FOUND for interpretation type pattern (''%s'')!', Type);
    else, error('NO MATCH FOUND for interpretation type pattern (''%s'')!', Type); end
    skipMatching = false; % False except true for ZInterpretation
    UnitKind = find(bw_find);
    switch(UnitKind),
        case 1, % SpaceInterpretation
            Units = {WITio.obj.wip.interpret_StandardUnit('m'), @(x) 1e-6.*x, @(y) 1e6.*y; ...
                WITio.obj.wip.interpret_StandardUnit('mm'),     @(x) 1e-3.*x, @(y) 1e3.*y; ...
                WITio.obj.wip.interpret_StandardUnit('µm'),     @(x) x,       @(y) y; ... % DEFAULT
                WITio.obj.wip.interpret_StandardUnit('nm'),     @(x) 1e3.*x,  @(y) 1e-3.*y; ...
                WITio.obj.wip.interpret_StandardUnit('Å'),      @(x) 1e4.*x,  @(y) 1e-4.*y; ...
                WITio.obj.wip.interpret_StandardUnit('pm'),     @(x) 1e6.*x,  @(y) 1e-6.*y};
            ValueUnit = Units{3,1}; % Default ValueUnit
        case 2, % SpectralInterpretation
            if isstruct(I), x0 = I.TDSpectralInterpretation.ExcitationWaveLength;
            else, x0 = I{2}; end
            % FOLLOWING EVALUATION CAN BE PERFORMANCE BOTTLENECK! (19.10.2017)
            Units = {WITio.obj.wip.interpret_StandardUnit('nm'),   @(x) x,                           @(y) y; ... % DEFAULT
                WITio.obj.wip.interpret_StandardUnit('µm'),        @(x) 1e-3.*x,                     @(y) 1e3.*y; ...
                WITio.obj.wip.interpret_StandardUnit('1/cm'),      @(x) 1e7./x,                      @(y) 1e7./y; ...
                WITio.obj.wip.interpret_StandardUnit('rel. 1/cm'), @(x) 1e7.*(1./x0-1./x),           @(y) 1./(1./x0-1e-7.*y); ...
                WITio.obj.wip.interpret_StandardUnit('eV'),        @(x) 1.23984193e3./x,             @(y) 1.23984193e3./y; ...
                WITio.obj.wip.interpret_StandardUnit('meV'),       @(x) 1.23984193e6./x,             @(y) 1.23984193e6./y; ...
                WITio.obj.wip.interpret_StandardUnit('rel. eV'),   @(x) -1.23984193e3.*(1./x0-1./x), @(y) 1./(1./x0+y./1.23984193e3); ...
                WITio.obj.wip.interpret_StandardUnit('rel. meV'),  @(x) -1.23984193e6.*(1./x0-1./x), @(y) 1./(1./x0+y./1.23984193e6)};
            ValueUnit = Units{1,1}; % Default ValueUnit
        case 3, % TimeInterpretation
            Units = {WITio.obj.wip.interpret_StandardUnit('h'), @(x) x./3600, @(y) 3600.*y; ...
                WITio.obj.wip.interpret_StandardUnit('min'),    @(x) x./60,   @(y) 60.*y; ...
                WITio.obj.wip.interpret_StandardUnit('s'),      @(x) x,       @(y) y; ... % DEFAULT
                WITio.obj.wip.interpret_StandardUnit('ms'),     @(x) 1e3.*x,  @(y) 1e-3.*y; ...
                WITio.obj.wip.interpret_StandardUnit('µs'),     @(x) 1e6.*x,  @(y) 1e-6.*y; ...
                WITio.obj.wip.interpret_StandardUnit('ns'),     @(x) 1e9.*x,  @(y) 1e-9.*y; ...
                WITio.obj.wip.interpret_StandardUnit('ps'),     @(x) 1e12.*x, @(y) 1e-12.*y; ...
                WITio.obj.wip.interpret_StandardUnit('fs'),     @(x) 1e15.*x, @(y) 1e-15.*y};
            ValueUnit = Units{3,1}; % Default ValueUnit
        case 4, % ZInterpretation
            if isstruct(I),
                x0 = I.TDZInterpretation.UnitName;
                % Try to recognize StandardUnit but allow non-StandardUnits
                x0 = WITio.obj.wip.interpret_StandardUnit(x0);
            elseif isnan(I{2}), x0 = WITio.obj.wip.ArbitraryUnit;
            else, x0 = WITio.obj.wip.interpret_StandardUnit(I{2}); end
            Units = {x0, @(x) x, @(y) y};
            skipMatching = true;
            ValueUnit = WITio.obj.wip.ArbitraryUnit; % Default ValueUnit
        case 5, % FrequencyInterpretation
            Units = {WITio.obj.wip.interpret_StandardUnit('µHz'),   @(x) 1e6.*x,    @(y) 1e-6.*y; ...
                WITio.obj.wip.interpret_StandardUnit('mHz'),        @(x) 1e3.*x,    @(y) 1e-3.*y; ...
                WITio.obj.wip.interpret_StandardUnit('Hz'),         @(x) x,         @(y) y; ... % DEFAULT
                WITio.obj.wip.interpret_StandardUnit('kHz'),        @(x) 1e-3.*x,   @(y) 1e3.*y; ...
                WITio.obj.wip.interpret_StandardUnit('MHz'),        @(x) 1e-6.*x,   @(y) 1e6.*y; ...
                WITio.obj.wip.interpret_StandardUnit('GHz'),        @(x) 1e-9.*x,   @(y) 1e9.*y; ...
                WITio.obj.wip.interpret_StandardUnit('THz'),        @(x) 1e-12.*x,  @(y) 1e12.*y};
            ValueUnit = Units{3,1}; % Default ValueUnit
        case 6, % InverseSpaceInterpretation
            Units = {WITio.obj.wip.interpret_StandardUnit('1/m'),   @(x) 1e6.*x,    @(y) 1e-6.*y; ...
                WITio.obj.wip.interpret_StandardUnit('1/mm'),       @(x) 1e3.*x,    @(y) 1e-3.*y; ...
                WITio.obj.wip.interpret_StandardUnit('1/µm'),       @(x) x,         @(y) y; ... % DEFAULT
                WITio.obj.wip.interpret_StandardUnit('1/nm'),       @(x) 1e-3.*x,   @(y) 1e3.*y; ...
                WITio.obj.wip.interpret_StandardUnit('1/Å'),        @(x) 1e-4.*x,   @(y) 1e4.*y; ...
                WITio.obj.wip.interpret_StandardUnit('1/pm'),       @(x) 1e-6.*x,   @(y) 1e6.*y};
            ValueUnit = Units{3,1}; % Default ValueUnit
        case 7, % PhaseInterpretation
            Units = {WITio.obj.wip.interpret_StandardUnit('rad'),   @(x) x,             @(y) y; ... % DEFAULT
                WITio.obj.wip.interpret_StandardUnit('mrad'),       @(x) 1e3.*x,        @(y) 1e-3.*y; ...
                WITio.obj.wip.interpret_StandardUnit('°'),          @(x) 180./pi.*x,    @(y) pi./180.*y; ...
                WITio.obj.wip.interpret_StandardUnit('grad'),       @(x) 200./pi.*x,    @(y) pi./200.*y; ...
                WITio.obj.wip.interpret_StandardUnit('mgrad'),      @(x) 2e5./pi.*x,    @(y) pi./2e5.*y};
            ValueUnit = Units{1,1}; % Default ValueUnit
    end
    
    % Interpret input (and convert OLD to DEFAULT)
    if ~isempty(Unit_old), % Interpret input if Unit_old is non-empty
        if ischar(Unit_old), % If given Unit_old is ValueUnit_old
            if ~skipMatching, % Skip only for ZInterpretation
                bw_find = strcmp(Units(:,1), WITio.obj.wip.interpret_StandardUnit(Unit_old)); % First test if a Standard Unit
                if sum(bw_find) ~= 1, bw_find = ~cellfun(@isempty, strfind(Units(:,1), Unit_old)); end % Otherwise widen the search
                if sum(bw_find) == 1, varargout = cellfun(Units{bw_find,3}, varargout, 'UniformOutput', false);
                elseif sum(bw_find) > 1, error('TWO OR MORE MATCHES FOUND for old unit pattern (''%s'')!', Unit_old);
                elseif isempty(strfind(PixelUnit, Unit_old)), error('NO MATCH FOUND for old unit pattern (''%s'')!', Unit_old); end
            end
        else, % If given Unit_old is UnitIndex_old
            if Unit_old >= 0 && Unit_old <= size(Units, 1)-1, % After this, change index notation to MATLAB convention
                varargout = cellfun(Units{Unit_old+1,3}, varargout, 'UniformOutput', false);
            else, error('Old unit index (%g) IS OUT OF RANGE [%g, %g]!', Unit_old, 0, size(Units, 1)-1); end
        end
    end
    
    % Interpret output (and convert DEFAULT to NEW)
    if ~isempty(Unit_new), % Interpret output if Unit_new is non-empty
        if ischar(Unit_new), % If given Unit_new is ValueUnit_new
            if skipMatching, % Only for ZInterpretation
                ValueUnit = WITio.obj.wip.interpret_StandardUnit(Unit_new);
            else, % Otherwise
                bw_find = strcmp(Units(:,1), WITio.obj.wip.interpret_StandardUnit(Unit_new)); % First test if a Standard Unit
                if sum(bw_find) ~= 1, bw_find = ~cellfun(@isempty, strfind(Units(:,1), Unit_new)); end % Otherwise widen the search
                if sum(bw_find) == 1,
                    ValueUnit = Units{bw_find,1};
                    varargout = cellfun(Units{bw_find,2}, varargout, 'UniformOutput', false);
                elseif sum(bw_find) > 1, error('TWO OR MORE MATCHES FOUND for new unit pattern (''%s'')!', Unit_new);
                elseif isempty(strfind(PixelUnit, Unit_new)), error('NO MATCH FOUND for new unit pattern (''%s'')!', Unit_new); end
            end
        else, % If given Unit_new is UnitIndex_new
            if Unit_new >= 0 && Unit_new <= size(Units, 1)-1, % After this, change index notation to MATLAB convention
                ValueUnit = Units{Unit_new+1,1};
                varargout = cellfun(Units{Unit_new+1,2}, varargout, 'UniformOutput', false);
            else, error('New unit index (%g) IS OUT OF RANGE [%g, %g]!', Unit_new, 0, size(Units, 1)-1); end
        end
    end
end
