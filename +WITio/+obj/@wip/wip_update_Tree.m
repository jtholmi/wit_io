% MIT No Attribution License (https://opensource.org/license/mit-0)
% 
% Copyright (c) 2019 Joonas T. Holmi (jtholmi@gmail.com), 2019-2021 Aalto University

% Update is always called when wip Project object may need to be updated
% with respect to its underlying wit Tree object.
% THIS DOES NOT SUPPORT WIP PROJECT OBJECT ARRAYS!
function wip_update_Tree(obj), %#ok
    Tree = obj.Tree;
    if Tree ~= Tree.Root, %#ok
        enableOnCleanup = disableObjectModified(Tree); % Temporarily disable the Project related wit-class ObjectModified events
        Tree.Root = Tree; % Prevent Root change for Root
        warning('Linked Project prevented Root change for its Tree!');
    elseif isempty(obj.TreeData), %#ok
        wip_update_Data(obj); % Check if Tree Data can be found
    end
end
