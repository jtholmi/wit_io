% MIT No Attribution License (https://opensource.org/license/mit-0)
% 
% Copyright (c) 2019 Joonas T. Holmi (jtholmi@gmail.com), 2019-2021 Aalto University

% Set Version if found
% THIS DOES NOT SUPPORT WIP PROJECT OBJECT ARRAYS!
function set_Root_Version(obj, Version) % Can be wid-, wip- or wit-class
    if ~isempty(obj),
        O_wit = WITio.obj.wit.empty; 
        if isa(obj, 'WITio.obj.wit'), O_wit = obj;
        elseif isa(obj, 'WITio.obj.wid') && ~isempty(obj.TreeDataPair), O_wit = obj.TreeDataPair(2).Root;
        elseif isa(obj, 'WITio.obj.wip'), O_wit = obj.Tree; end
        if ~isempty(O_wit), % Test if a tree exists
            Version_tag = O_wit.Root.search('Version', {'WITec (Project|Data)'});
            if ~isempty(Version_tag), % Test if a Version-tag was found
                Version_tag.Data = int32(Version);
            end
        end
    end
end
