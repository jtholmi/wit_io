% MIT No Attribution License (https://opensource.org/license/mit-0)
% 
% Copyright (c) 2019 Joonas T. Holmi (jtholmi@gmail.com), 2019-2021 Aalto University
% 
% Class for project
classdef wip < handle, % Since R2008a
    %% MAIN PROPERTIES
    properties (SetAccess = private, Dependent) % READ-ONLY, DEPENDENT
        File;
        Name;
    end
    properties (SetAccess = private) % READ-ONLY
        Data = WITio.obj.wid.empty;
    end
    properties (Dependent) % READ-WRITE, DEPENDENT
        Type;
    end
    
    %% OTHER PROPERTIES
    properties (Dependent) % READ-WRITE, DEPENDENT
        Version; % WIP/WID-file version. See 'README on WIT-tag formatting.txt'.
    end
    properties (SetAccess = private) % READ-ONLY
        Tree = WITio.obj.wit.empty;
        TreeData = WITio.obj.wit.empty;
    end
    
    properties (SetAccess = private, Hidden) % READ-ONLY, HIDDEN
        TreeDataModifiedCount = [];
        wip_listener;
        TreeObjectBeingDestroyedListener;
        TreeObjectModifiedListener;
        DataObjectBeingDestroyedListener;
        DataObjectModifiedListener;
    end
    
    properties % READ-WRITE
        % DataUnit, FrequencyUnit, InverseSpaceUnit, PhaseUnit, SpaceUnit, SpectralUnit or TimeUnit
        ForceDataUnit = '';
        ForceFrequencyUnit = '';
        ForceInverseSpaceUnit = '';
        ForcePhaseUnit = '';
        ForceSpaceUnit = '';
        ForceSpectralUnit = '';
        ForceTimeUnit = '';
    end
    
    % The following properties are shared by all wip Project objects!
    properties (Dependent) % READ-WRITE, DEPENDENT
        % Configure interaction with wid-class methods
        AutoCopyObj; % Automatically make a copy of the original object (whenever applicable). If false, then obj output should be originals.
        AutoCreateObj; % Automatically create a new object (whenever applicable). If false, then new_obj output should be empty.
        AutoModifyObj; % Automatically modify either the original object or its copy (whenever applicable). If false, then obj output should not be modified.
        AutoNanInvalid; % A feature of TDGraph and TDImage. If used, shows NaN where invalid.
        % Configure file writing behaviour
        AutoDestroyDuplicateTransformations; % If true, then removes all the duplicate Transformations (and keeps the first one).
        AutoDestroyViewers; % If true, then removes all the Viewer windows (shown on the WITec software side). This avoids possible corruption of the modified files, because WITio mostly ignores Viewers.
    end
    
    properties (Constant) % READ-ONLY, CONSTANT
        FullStandardUnits = {'Arbitrary Unit (a.u.)', ...
            'Degrees (°)', 'Energy (eV)', 'Energy (meV)', ...
            'Femtoseconds (fs)', 'Gigahertz (GHz)', 'Gradians (grad)', ...
            'Hertz (Hz)', 'Hours (h)', 'Kilohertz (kHz)', ...
            'Megahertz (MHz)', 'Meters (m)', 'Microhertz (µHz)', ...
            'Micrometers (µm)', 'Microseconds (µs)', ...
            'Milligradians (mgrad)', 'Millihertz (mHz)', ...
            'Millimeters (mm)', 'Milliradians (mrad)', ...
            'Milliseconds (ms)', 'Minutes (min)', 'Nanometers (nm)', ...
            'Nanoseconds (ns)', 'Picometers (pm)', 'Picoseconds (ps)', ...
            'Radians (rad)', 'Raman Shift (rel. 1/cm)', ...
            'Reciprocal Meters (1/m)', 'Reciprocal Micrometers (1/µm)', ...
            'Reciprocal Millimeters (1/mm)', ...
            'Reciprocal Nanometers (1/nm)', ...
            'Reciprocal Picometers (1/pm)', ...
            'Reciprocal Ångströms (1/Å)', ...
            'Relative Energy (rel. eV)', 'Relative Energy (rel. meV)', ...
            'Seconds (s)', 'Spectroscopic Wavenumber (1/cm)', ...
            'Terahertz (THz)', 'Ångströms (Å)'};
        ArbitraryUnit = WITio.obj.wip.interpret_StandardUnit('a.u.');
        DefaultFrequencyUnit = WITio.obj.wip.interpret_StandardUnit('Hz');
        DefaultInverseSpaceUnit = WITio.obj.wip.interpret_StandardUnit('1/µm');
        DefaultPhaseUnit = WITio.obj.wip.interpret_StandardUnit('rad');
        DefaultSpaceUnit = WITio.obj.wip.interpret_StandardUnit('µm');
        DefaultSpectralUnit = WITio.obj.wip.interpret_StandardUnit('nm');
        DefaultTimeUnit = WITio.obj.wip.interpret_StandardUnit('s');
    end
    
    properties (Constant) % READ-ONLY, CONSTANT
        Projects = WITio.obj.handle_listener(); % Generates a shared Project listener
    end
    
    %% PUBLIC METHODS
    methods
        % CONSTRUCTOR
        function obj = wip(TreeOrData),
            if nargin == 0, TreeOrData = WITio.obj.wip.new(); end % Create minimal project
            
            % SPECIAL CASE: Empty wip object
            if isempty(TreeOrData),
                obj = obj([]); % WITio.obj.wip.empty
                return;
            end
            
            try,
                % Validate the given input
                if isa(TreeOrData, 'WITio.obj.wit') && numel(TreeOrData) == 1,
                    Tree = TreeOrData.Root;
                elseif isa(TreeOrData, 'WITio.obj.wid'),
                    Data = TreeOrData;
                    Pairs = cat(1, Data.TreeDataPair);
                    Roots = unique([Pairs(:,2).Root]);
                    if numel(Roots) ~= 1,
                        error('Provide a wid Data object array with only one common Root!');
                    end
                    Tree = Roots;
                else,
                    error('Provide either a wit Tree object or a wid Data object array!');
                end
                
                % Check whether or not similar project already exists!
                O_wip = WITio.obj.wip.Projects.match_any(@(O_wip) O_wip.Tree == Tree); % Due to syncronous link between Project and its Tree, Project's Tree is always Root AND never invalid/deleted!
                
                % Use already existing wip Project object or this
                if ~isempty(O_wip),
                    obj = O_wip;
                else,
                    obj.Tree = Tree;
                    % Enable tracking of this wip Project object
                    obj.wip_listener = WITio.obj.wip.Projects.add(obj);
                    % Get user preferences (or default values if not found)
                    obj.ForceDataUnit = WITio.tbx.pref.get('wip_ForceDataUnit', obj.ForceDataUnit);
                    obj.ForceFrequencyUnit = WITio.tbx.pref.get('wip_ForceFrequencyUnit', obj.ForceFrequencyUnit);
                    obj.ForceInverseSpaceUnit = WITio.tbx.pref.get('wip_ForceInverseSpaceUnit', obj.ForceInverseSpaceUnit);
                    obj.ForcePhaseUnit = WITio.tbx.pref.get('wip_ForcePhaseUnit', obj.ForcePhaseUnit);
                    obj.ForceSpaceUnit = WITio.tbx.pref.get('wip_ForceSpaceUnit', obj.ForceSpaceUnit);
                    obj.ForceSpectralUnit = WITio.tbx.pref.get('wip_ForceSpectralUnit', obj.ForceSpectralUnit);
                    obj.ForceTimeUnit = WITio.tbx.pref.get('wip_ForceTimeUnit', obj.ForceTimeUnit);
                    % Enable link between Tree and Project
                    obj.TreeObjectBeingDestroyedListener = Tree.addlistener('ObjectBeingDestroyed', @(s,e) delete(obj));
                    obj.TreeObjectModifiedListener = Tree.addlistener('ObjectModified', @(s,e) wip_update_Tree(obj));
                    wip_update_Tree(obj);
                end
            catch me, % Handle invalid or deleted object -case
                switch me.identifier,
                    case 'MATLAB:class:InvalidHandle', obj = obj([]); % WITio.obj.wid.empty
                    otherwise, rethrow(me);
                end
            end
        end
        
        function delete(obj),
            try, % Delete event listeners before toughing the wit Tree
                delete(obj.TreeObjectBeingDestroyedListener);
                delete(obj.TreeObjectModifiedListener);
                delete(obj.DataObjectBeingDestroyedListener);
                delete(obj.DataObjectModifiedListener);
            catch, return; end % Do nothing if already deleted (backward compatible with R2011a)
            % Delete the underlying wit Tree objects
            delete(obj.Tree);
            % Delete the underlying wid Data objects
            delete(obj.Data);
            % Useful resources:
            % https://se.mathworks.com/help/matlab/matlab_oop/handle-class-destructors.html
            % https://se.mathworks.com/help/matlab/matlab_oop/example-implementing-linked-lists.html
            % https://blogs.mathworks.com/loren/2013/07/23/deconstructing-destructors/
        end
        
        
        
        %% MAIN PROPERTIES
        % File (READ-ONLY, DEPENDENT)
        function File = get.File(obj),
            File = obj.Tree.File;
        end
        
        % Name (READ-ONLY, DEPENDENT)
        function Name = get.Name(obj),
            [~, name, ext] = fileparts(obj.Tree.File);
            Name = [name ext];
        end
        
        % Data (READ-ONLY)
        function Data = get.Data(obj),
            Data = obj.Data; % Auto-updated by an event listener
        end
        
        % Type (READ-WRITE, DEPENDENT)
        function Type = get.Type(obj),
            Type = obj.Tree.Name;
        end
        function set.Type(obj, Type),
            % Validate the given input
            if ischar(Type), Type = reshape(Type, 1, []);
            else, error('Only a char array can be a type!'); end
            
            % Do nothing if no change
            if strcmp(Type, obj.Tree.Name), return; end
            
            % Sort prior children by names
            Tree_prior = obj.Tree;
            Children_prior = [Tree_prior.Children WITio.obj.wit.empty];
            [names_prior, ind_prior] = sort({Children_prior.Name});
            Children_prior = Children_prior(ind_prior);
            
            % Generate new tree structure template
            Tree = WITio.obj.wit.empty;
            switch(Type),
                case 'WITec Project',
                    Tree = WITio.obj.wip.new(obj.Version);
                case 'WITec Data',
                    Tree = WITio.obj.wid.new(obj.Version);
                otherwise,
                    error('Unimplemented Type (%s)! Type must be either ''WITec Project'' or ''WITec Data''.', Type);
            end
            
            % Use the new tree structure if generated
            if ~isempty(Tree),
                % Sort posterior children by names
                Children = [Tree.Children WITio.obj.wit.empty];
                [names, ind] = sort({Children.Name});
                Children = Children(ind);
                % Find prior children by matching child names and adopt them
                jj_begin = 1;
                for ii = 1:numel(names),
                    for jj = jj_begin:numel(names_prior),
                        if strcmp(names{ii}, names_prior{jj}),
                            % Adopt the prior child and destroy template
                            delete(Children(ii));
                            Children(ii) = Children_prior(jj);
                            break;
                        elseif strcmp(names{ii}, 'NextDataID'),
                            offset = max([Tree_prior.regexp('^ID<TData<Data \d+(<Data)?$').Data])+1;
                            if isempty(offset), offset = 1; end % No data yet
                            Children(ii).Data = int32(offset);
                        end
                    end
                    jj_begin = jj+1; % Due to sorting, we can skip the previously matched prior children
                end
                % Temporarily disable the Project related TreeData ObjectModified events
                enableOnCleanup = disableObjectModified(obj.TreeData);
                % Permanently disable the Project related Tree events
                delete(obj.TreeObjectBeingDestroyedListener);
                delete(obj.TreeObjectModifiedListener);
                % Update posterior tree children
                Children(ind) = Children; % Unsort posterior children
                Tree.Data = Children;
                % Use the new tree and destroy the old tree
                obj.Tree = Tree;
                delete(Tree_prior); % Destroy whatever remains of the old tree
                % Re-enable link between Tree and Project
                obj.TreeObjectBeingDestroyedListener = Tree.addlistener('ObjectBeingDestroyed', @(s,e) delete(obj));
                obj.TreeObjectModifiedListener = Tree.addlistener('ObjectModified', @(s,e) wip_update_Tree(obj));
            end
        end
        
        %% OTHER PROPERTIES
        % Version (READ-WRITE, DEPENDENT)
        function Version = get.Version(obj),
            Version = WITio.obj.wip.get_Root_Version(obj.Tree);
        end
        function set.Version(obj, Version),
            WITio.obj.wip.set_Root_Version(obj.Tree, Version);
        end
        
        % Tree (READ-ONLY)
        function Tree = get.Tree(obj),
            Tree = obj.Tree; % Auto-updated by an event listener
        end
        
        % ForceDataUnit (READ-WRITE)
        function set.ForceDataUnit(obj, Value),
            if ~isempty(Value),
                try, Value = WITio.obj.wip.interpret('TDZInterpretation', Value); % Try interpret
                catch, Value = ''; end % Reset to empty on failure
            end
            obj.ForceDataUnit = Value;
        end
        
        % ForceFrequencyUnit (READ-WRITE)
        function set.ForceFrequencyUnit(obj, Value),
            if ~isempty(Value),
                try, Value = WITio.obj.wip.interpret('TDFrequencyInterpretation', Value); % Try interpret
                catch, Value = ''; end % Reset to empty on failure
            end
            obj.ForceFrequencyUnit = Value;
        end
        
        % ForceInverseSpaceUnit (READ-WRITE)
        function set.ForceInverseSpaceUnit(obj, Value),
            if ~isempty(Value),
                try, Value = WITio.obj.wip.interpret('TDInverseSpaceInterpretation', Value); % Try interpret
                catch, Value = ''; end % Reset to empty on failure
            end
            obj.ForceInverseSpaceUnit = Value;
        end
        
        % ForcePhaseUnit (READ-WRITE)
        function set.ForcePhaseUnit(obj, Value),
            if ~isempty(Value),
                try, Value = WITio.obj.wip.interpret('TDPhaseInterpretation', Value); % Try interpret
                catch, Value = ''; end % Reset to empty on failure
            end
            obj.ForcePhaseUnit = Value;
        end
        
        % ForceSpaceUnit (READ-WRITE)
        function set.ForceSpaceUnit(obj, Value),
            if ~isempty(Value),
                try, Value = WITio.obj.wip.interpret('TDSpaceInterpretation', Value); % Try interpret
                catch, Value = ''; end % Reset to empty on failure
            end
            obj.ForceSpaceUnit = Value;
        end
        
        % ForceSpectralUnit (READ-WRITE)
        function set.ForceSpectralUnit(obj, Value),
            if ~isempty(Value),
                try, Value = WITio.obj.wip.interpret('TDSpectralInterpretation', Value); % Try interpret
                catch, Value = ''; end % Reset to empty on failure
            end
            obj.ForceSpectralUnit = Value;
        end
        
        % ForceTimeUnit (READ-WRITE)
        function set.ForceTimeUnit(obj, Value),
            if ~isempty(Value),
                try, Value = WITio.obj.wip.interpret('TDTimeInterpretation', Value); % Try interpret
                catch, Value = ''; end % Reset to empty on failure
            end
            obj.ForceTimeUnit = Value;
        end
        
        
        
        %% OTHER METHODS
        function value = get.AutoCopyObj(~), value = WITio.tbx.pref.get('wip_AutoCopyObj', true); end % With default
        function set.AutoCopyObj(~, value), WITio.tbx.pref.set('wip_AutoCopyObj', value); end
        function value = get.AutoCreateObj(~), value = WITio.tbx.pref.get('wip_AutoCreateObj', true); end % With default
        function set.AutoCreateObj(~, value), WITio.tbx.pref.set('wip_AutoCreateObj', value); end
        function value = get.AutoModifyObj(~), value = WITio.tbx.pref.get('wip_AutoModifyObj', true); end % With default
        function set.AutoModifyObj(~, value), WITio.tbx.pref.set('wip_AutoModifyObj', value); end
        function value = get.AutoNanInvalid(~), value = WITio.tbx.pref.get('wip_AutoNanInvalid', true); end % With default
        function set.AutoNanInvalid(~, value), WITio.tbx.pref.set('wip_AutoNanInvalid', value); end
        function value = get.AutoDestroyDuplicateTransformations(~), value = WITio.tbx.pref.get('wip_AutoDestroyDuplicateTransformations', true); end % With default
        function set.AutoDestroyDuplicateTransformations(~, value), WITio.tbx.pref.set('wip_AutoDestroyDuplicateTransformations', value); end
        function value = get.AutoDestroyViewers(~), value = WITio.tbx.pref.get('wip_AutoDestroyViewers', true); end % With default
        function set.AutoDestroyViewers(~, value), WITio.tbx.pref.set('wip_AutoDestroyViewers', value); end
        
        
        
        % Open Project Manager with the given parameters
        O_wid = manager(obj, varargin);
        
        % File writer
        write(obj, varargin);
        
        % Destroy duplicate transformations 
        destroy_duplicate_Transformations(obj);
        
        % Remove saved Viewer-settings
        destroy_Viewers(obj);
        
        % Helper functions for adding, removing and finding wid-objects
        O_wid = find_Data(obj, ID);
        
        % Transformations and interpretations (project version)
        [ValueUnit, varargout] = transform_forced(obj, S, varargin);
        [ValueUnit, varargout] = interpret_forced(obj, S, Unit_new, Unit_old, varargin);
    end
    
    %% PRIVATE METHODS
    methods (Access = private)
        % Update Tree- and Data-properties according to wit Tree object changes
        wip_update_Tree(obj);
        wip_update_Data(obj);
    end
    
    methods (Static)
        % Constructor WIP-formatted WIT-tree
        O_wit = new(Version); % WITec Project WIT-tree
        O_wit = new_TData(Version, Caption); % Only TData WIT-tree
        
        % Get valid pairs within the given wit Tree objects
        [Pairs, Roots] = get_Data_DataClassName_pairs(O_wit);
        [Pairs, Roots] = get_Viewer_ViewerClassName_pairs(O_wit);
        
        % Appender of multiple WIT-trees (or Projects)
        [O_wit, varargout] = append(varargin);
        
        % File reader
        [O_wid, O_wip, O_wit] = read(varargin);
        Version = read_Version(File);
        
        % File version
        Version = get_Root_Version(obj); % Can be wid-, wip- or wit-class
        set_Root_Version(obj, Version); % Can be wid-, wip- or wit-class
        
        % Transformations and interpretations (static version)
        [ValueUnit, varargout] = transform(S, varargin);
        [ValueUnit, varargout] = interpret(S, Unit_new, Unit_old, varargin);
        ValueUnit = interpret_StandardUnit(StandardUnit);
    end
end
