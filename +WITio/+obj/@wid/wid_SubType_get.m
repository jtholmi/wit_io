% MIT No Attribution License (https://opensource.org/license/mit-0)
% 
% Copyright (c) 2019 Joonas T. Holmi (jtholmi@gmail.com), 2019-2021 Aalto University

function out = wid_SubType_get(obj),
    out = '';
    switch(obj.Type),
        case 'TDBitmap',
            out = 'Image';
%             if size(obj.Data, 4) > 1, out = 'Volume'; end % CUSTOM (but very slow due to obj.Data-call!)
        case 'TDGraph',
            switch(obj.ImageIndex), % VERIFIED 25.7.2016 TO BE THE COMPLETE LIST!
                case 0, out = 'Image';
%                     if size(obj.Data, 4) > 1, out = 'Volume'; end % CUSTOM (but very slow due to obj.Data-call!)
                case 1, out = 'Line';
                case 2, out = 'Point';
                case 3, out = 'Array';
                case 4, out = 'Histogram';
                case 5, out = 'Time';
                case 6, out = 'Mask';
            end
        case 'TDImage',
            out = 'Image';
%             if size(obj.Data, 4) > 1, out = 'Volume'; end % CUSTOM (but very slow due to obj.Data-call!)
        case 'TDText', out = 'Text';
        case 'TDLinearTransformation', out = 'Linear';
        case 'TDLUTTransformation', out = 'LookUpTable';
        case 'TDSpaceTransformation',
            out = 'Space'; % By default in case of failures
            UnitKind = obj.TreeDataPair.search_children('TDTransformation').search_children('UnitKind');
            if ~isempty(UnitKind) && UnitKind.Data == 6,
                out = 'InverseSpace';
            end
        case 'TDSpectralTransformation', out = 'Spectral';
        case 'TDFrequencyInterpretation', out = 'Frequency'; % If in manager
        case 'TDInverseSpaceInterpretation', out = 'InverseSpace'; % I/x in manager
        case 'TDPhaseInterpretation', out = 'Phase'; % Io in manager
        case 'TDSpaceInterpretation', out = 'Space'; % Ix in manager
        case 'TDSpectralInterpretation', out = 'Spectral'; % Iλ in manager
        case 'TDTimeInterpretation', out = 'Time'; % It in manager
        case 'TDZInterpretation', out = 'Data'; % Iz in manager
    end
end
