% MIT No Attribution License (https://opensource.org/license/mit-0)
% 
% Copyright (c) 2019 Joonas T. Holmi (jtholmi@gmail.com), 2019-2021 Aalto University

function [new_obj, Min] = filter_min(obj, varargin),
    fun = @(I, X, dim) min(I, [], dim);
    str_fun = 'Min';
    [new_obj, Min] = obj.filter_fun(fun, str_fun, varargin{:});
end
