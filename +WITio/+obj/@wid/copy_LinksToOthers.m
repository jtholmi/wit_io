% MIT No Attribution License (https://opensource.org/license/mit-0)
% 
% Copyright (c) 2019 Joonas T. Holmi (jtholmi@gmail.com), 2019-2021 Aalto University

function copy_LinksToOthers(obj),
    % Temporarily disable the Project related wit-class ObjectModified events until the end of the function
    enableOnCleanup = disableObjectModified([obj.TreeDataPair(1).Root obj.TreeDataPair]);
    
    % DANGER! Call this function only if obj and its obj.Tag are COPIES!
    % Handle transformations and interpretations
    Tag_Id = obj.TreeDataPair(2).regexp('^[^<]+ID(List)?(<[^<]*)*$'); % Should not match with ID<TData!
    for ii = 1:numel(Tag_Id),
        ids_ii = Tag_Id(ii).Data;
        for jj = 1:numel(ids_ii),
            if ids_ii(jj) ~= 0, % Ignore if zero
                Link = obj.Project.find_Data(ids_ii(jj));
                Link_new = Link.copy();
                Tag_Id(ii).Data(jj) = Link_new.Id; % DANGER! This modifies obj.Tag!
            end
        end
    end
end
