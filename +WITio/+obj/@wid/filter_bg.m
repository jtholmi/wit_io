% MIT No Attribution License (https://opensource.org/license/mit-0)
% 
% Copyright (c) 2019 Joonas T. Holmi (jtholmi@gmail.com), 2019-2021 Aalto University

function [obj, Data_range, Graph_range, Data_range_bg] = filter_bg(obj, varargin),
    [Data_range, Graph_range, Data_range_bg, range] = WITio.obj.wid.crop_Graph_with_bg_helper(obj.Data, obj.Info.Graph, varargin{:});
    
    obj = obj.crop_Graph([], Data_range, Graph_range);
    
    % Modify the object (or its copy) if permitted
    if WITio.tbx.pref.get('wip_AutoModifyObj', true),
        obj.Name = sprintf('[%g-%g]<%s', range(1), range(2), obj.Name);
    end
end
