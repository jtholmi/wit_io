% MIT No Attribution License (https://opensource.org/license/mit-0)
% 
% Copyright (c) 2019 Joonas T. Holmi (jtholmi@gmail.com), 2019-2021 Aalto University

% Helper function to SET (un)formatted struct-content to wit-tree.
function wid_DataTree_set(obj, in, varargin),
    WITio.obj.wit.DataTree_set(obj.TreeDataPair(2), in, varargin{:});
end
