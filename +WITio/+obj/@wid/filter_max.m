% MIT No Attribution License (https://opensource.org/license/mit-0)
% 
% Copyright (c) 2019 Joonas T. Holmi (jtholmi@gmail.com), 2019-2021 Aalto University

function [new_obj, Max] = filter_max(obj, varargin),
    fun = @(I, X, dim) max(I, [], dim);
    str_fun = 'Max';
    [new_obj, Max] = obj.filter_fun(fun, str_fun, varargin{:});
end
