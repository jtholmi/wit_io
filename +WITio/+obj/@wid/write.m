% MIT No Attribution License (https://opensource.org/license/mit-0)
% 
% Copyright (c) 2019 Joonas T. Holmi (jtholmi@gmail.com), 2019-2021 Aalto University

% CAUTION: When writing multiple objects of multiple Versions to a single
% WID-file, then this MAY produce corrupted files if Versions are
% incompatible with each other!
function write(obj, File) % For saving WIT-formatted WID-files!
    if nargin < 2 || iscell(File), % If no or many filenames specified
        for ii = 1:numel(obj),
            if nargin < 2 || isempty(File{ii}), File{ii} = obj(ii).TreeDataPair(2).File; end
            O_wit = WITio.obj.wid.new(WITio.obj.wip.get_Root_Version(obj(ii))); % Create minimal data for each object
            if ~isempty(obj(ii).TreeDataPair),
                O_wits = obj(ii).TreeDataPair;
            end
            AllLinksToOthers = struct2cell(obj(ii).AllLinksToOthers);
            for jj = 1:numel(AllLinksToOthers), % Also add the linked object such as transformations and interpretations
                if ~isempty(AllLinksToOthers{jj}.TreeDataPair),
                    O_wits = [O_wits AllLinksToOthers{jj}.TreeDataPair];
                end
            end
            O_wit = WITio.obj.wip.append(O_wit, unique(O_wits));
            O_wit.write(File{ii});
            delete(O_wit);
        end
    elseif ischar(File), % If only one filename specified for all, then save all to the same
        Version = WITio.obj.wip.get_Root_Version(obj(1));
        if numel(obj) > 0, O_wit = WITio.obj.wid.new(Version);
        else, O_wit = WITio.obj.wid.new(); end % Create minimal data for all objects
        O_wits = WITio.obj.wit.empty;
        for ii = 1:numel(obj),
            if WITio.obj.wip.get_Root_Version(obj(ii)) ~= Version,
                warning('Object with index ii has mismatching Version numbering.', ii);
            end
            if ~isempty(obj(ii).TreeDataPair),
                O_wits = [O_wits obj(ii).TreeDataPair];
            end
            AllLinksToOthers = struct2cell(obj(ii).AllLinksToOthers);
            for jj = 1:numel(AllLinksToOthers), % Also add the linked object such as transformations and interpretations
                if ~isempty(AllLinksToOthers{jj}) && ~isempty(AllLinksToOthers{jj}.TreeDataPair),
                    O_wits = [O_wits AllLinksToOthers{jj}.TreeDataPair];
                end
            end
        end
        
        % Remove duplicates
        [~, ia] = unique([O_wits.Id]); % This should be deterministic, whereas the simple unique of object handles can be non-deterministic!
        O_wits = O_wits(ia);
        
        O_wit = WITio.obj.wip.append(O_wit, O_wits);
        O_wit.write(File);
        delete(O_wit);
    end
end
