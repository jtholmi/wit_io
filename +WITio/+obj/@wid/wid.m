% MIT No Attribution License (https://opensource.org/license/mit-0)
% 
% Copyright (c) 2019 Joonas T. Holmi (jtholmi@gmail.com), 2019-2021 Aalto University
% 
% Class for project datas
classdef wid < handle, % Since R2008a
    %% MAIN PROPERTIES
    % Everything rely on the underlying wit-classes
    properties (SetAccess = private, Dependent) % READ-ONLY, DEPENDENT
        File; % Data-specific full file name
    end
    properties (Dependent) % READ-WRITE, DEPENDENT
        Name;
        Data;
        Type;
    end
    
    %% OTHER PROPERTIES
    % Everything rely on the underlying wit-classes
    properties (Dependent) % READ-WRITE, DEPENDENT
        Version;
    end
    properties (SetAccess = private, Dependent) % READ-WRITE, DEPENDENT
        Info;
    end
    properties (Dependent) % READ-WRITE, DEPENDENT
        DataTree;
        Id;
        ImageIndex;
        OrdinalNumber;
        SubType;
    end
    properties (SetAccess = private, Dependent) % READ-ONLY, DEPENDENT
        LinksToOthers;
        AllLinksToOthers;
        LinksToThis;
        AllLinksToThis;
        Tag; % Either empty OR contains all the fields defined in wid-constructor
    end
    properties (SetAccess = private) % READ-ONLY
        TreeDataPair = WITio.obj.wit.empty;
        Project = WITio.obj.wip.empty;
    end
    
    %% PUBLIC METHODS
    methods
        % CONSTRUCTOR
        function obj = wid(SizeOrTreeOrProject),
            % Loops through each element in O_wit array. It checks whether
            % the element points directly to a specific Data/DataClassName
            % (or its children) or not. If yes, then it only adds that as
            % new wid. If no, then it adds all found Data/DataClassName
            % pairs as new wid.
            
            persistent isSize;
            if isempty(isSize), isSize = false; end
            
            if nargin == 0, % Create minimal TDGraph data
                if ~isSize, obj = WITio.obj.wid.new_Graph(); end
                return;
            end
            
            % SPECIAL CASE: Empty wid object
            if isempty(SizeOrTreeOrProject),
                obj = obj([]); % WITio.obj.wid.empty
                return;
            elseif isnumeric(SizeOrTreeOrProject),
                isSize = true;
                if numel(SizeOrTreeOrProject) == 1, SizeOrTreeOrProject(2) = SizeOrTreeOrProject; end
                obj(prod(SizeOrTreeOrProject),1) = WITio.obj.wid();
                obj = reshape(obj, SizeOrTreeOrProject);
                isSize = false;
                return;
            end
            
            try,
                % Validate the given input
                if isa(SizeOrTreeOrProject, 'WITio.obj.wit'),
                    % Also used by wip_update_Data
                    Project = WITio.obj.wip.empty;
                    Tree = SizeOrTreeOrProject;
                    Roots = unique([Tree.Root]);
                    if numel(Roots) ~= 1,
                        error('Provide a wit Tree object array with only one common Root!');
                    end
                elseif isa(SizeOrTreeOrProject, 'WITio.obj.wip') && numel(SizeOrTreeOrProject) == 1,
                    % Also used by wip_update_Data
                    Project = SizeOrTreeOrProject; % Never empty
                    Tree = Project.Tree;
                    Roots = Tree;
                else,
                    error('Provide either a wit Tree object array or a wip Project object!');
                end

                % Get valid tag pairs
                Pairs = WITio.obj.wip.get_Data_DataClassName_pairs(Tree);
                
                % Stop if no valid pairs found
                N_pairs = size(Pairs, 1);
                if N_pairs == 0,
                    obj = obj([]); % WITio.obj.wid.empty
                    return;
                end

                % If empty Project, then find or create Project
                % Also used by wip_update_Data
                if isempty(Project), Project = WITio.obj.wip(Roots); end
                
                % Use Project Data if already exists and stop
                if ~isempty(Project.Data),
                    % Skip for wip_update_Data
                    OrdinalNumbers = cellfun(@(s) sscanf(s, '%*s %d'), {Pairs(:,1).Name});
                    obj = Project.Data(OrdinalNumbers+1);
                    return
                end

                % Otherwise, create Project Data
                % Also used by wip_update_Data

                % Loop the found pairs to construct wids
                obj = WITio.obj.wid([N_pairs 1]); % Preallocate the array first
                for ii = 1:N_pairs,
                    obj(ii).TreeDataPair = Pairs(ii,:);
                    obj(ii).Project = Project;
                end
            catch me, % Handle invalid or deleted object -case
                switch me.identifier,
                    case 'MATLAB:class:InvalidHandle', obj = obj([]); % WITio.obj.wid.empty
                    otherwise, rethrow(me);
                end
            end
        end
        
        function delete(obj),
            % Delete its tree branches
            try, delete(obj.TreeDataPair); catch, end % Backward compatibility with R2011a to avoid 'Invalid or deleted object.'
            % Useful resources:
            % https://se.mathworks.com/help/matlab/matlab_oop/handle-class-destructors.html
            % https://se.mathworks.com/help/matlab/matlab_oop/example-implementing-linked-lists.html
            % https://blogs.mathworks.com/loren/2013/07/23/deconstructing-destructors/
        end
        
        % Quicker delete-method for sibling wid Data objects
        function delete_siblings(obj), %#ok
            [Project, ~, ic] = unique([obj.Project]); %#ok
            if numel(Project) ~= 1 || numel(ic) ~= numel(obj), %#ok
                error('The given objects must be of the same wip Project!');
            end
            
            % Delete their tree branches at once (for speed)
            delete_siblings([obj.TreeDataPair]);
        end
        
        
        
        %% MAIN PROPERTIES
        % File (READ-ONLY, DEPENDENT)
        function File = get.File(obj),
            File = '';
            if ~isempty(obj.TreeDataPair),
                % This will always show obj.TreeDataPair(2).Root.File, even though its
                % obj.TreeDataPair(2).File may differ due to obj-copying or low-
                % memory-mode. The root's File is needed to show correct
                % file ownership, e.g., in the Project Manager.
                File = obj.TreeDataPair(2).Root.File;
            end
        end
        
        % Name (READ-WRITE, DEPENDENT)
        function Name = get.Name(obj),
            Name = '';
            Tag = obj.Tag;
            if ~isempty(Tag) && ~isempty(Tag.Caption),
                Name = Tag.Caption.Data;
            end
        end
        function set.Name(obj, Name),
            Tag = obj.Tag;
            if ~isempty(Tag) && ~isempty(Tag.Caption),
                Tag.Caption.Data = char(Name);
            end
        end
        
        % Data (READ-WRITE, DEPENDENT)
        function Data = get.Data(obj),
            Data = [];
            if ~isempty(obj.TreeDataPair),
                Data = obj.wid_Data_get();
            end
        end
        function set.Data(obj, Data),
            if ~isempty(obj.TreeDataPair),
                obj.wid_Data_set(Data);
            end
        end
        
        % Type (READ-WRITE, DEPENDENT)
        function Type = get.Type(obj),
            Type = '';
            if ~isempty(obj.TreeDataPair),
                Type = obj.TreeDataPair(1).Data;
            end
        end
        function set.Type(obj, Type),
            if ~isempty(obj.TreeDataPair),
                obj.TreeDataPair(1).Data = char(Type);
            end
        end
        
        %% OTHER PROPERTIES
        % Version (READ-WRITE, DEPENDENT)
        function Version = get.Version(obj),
            Version = [];
            Tag = obj.Tag;
            if ~isempty(Tag) && ~isempty(Tag.RootVersion),
                Version = Tag.RootVersion.Data;
            end
        end
        function set.Version(obj, Version),
            Tag = obj.Tag;
            if ~isempty(Tag) && ~isempty(Tag.RootVersion),
                Tag.RootVersion.Data = int32(Version);
            end
        end
        
        % Info (READ-ONLY, DEPENDENT)
        function Info = get.Info(obj),
            Info = obj.wid_Info_get();
        end
        
        % DataTree (READ-WRITE, DEPENDENT)
        function DataTree = get.DataTree(obj),
            DataTree = struct.empty;
            if ~isempty(obj.TreeDataPair),
                DataTree = obj.wid_DataTree_get();
            end
        end
        function set.DataTree(obj, DataTree),
            if ~isempty(obj.TreeDataPair),
                obj.wid_DataTree_set(DataTree);
            end
        end
        
        % Id (READ-WRITE, DEPENDENT)
        function Id = get.Id(obj),
            Id = [];
            Tag = obj.Tag;
            if ~isempty(Tag) && ~isempty(Tag.Id),
                Id = Tag.Id.Data;
            end
        end
        function set.Id(obj, Id),
            Tag = obj.Tag;
            if ~isempty(Tag) && ~isempty(Tag.Id),
                Tag.Id.Data = int32(Id);
            end
        end
        
        % ImageIndex (READ-WRITE, DEPENDENT)
        function ImageIndex = get.ImageIndex(obj),
            ImageIndex = [];
            Tag = obj.Tag;
            if ~isempty(Tag) && ~isempty(Tag.ImageIndex),
                ImageIndex = Tag.ImageIndex.Data;
            end
        end
        function set.ImageIndex(obj, ImageIndex),
            Tag = obj.Tag;
            if ~isempty(Tag) && ~isempty(Tag.ImageIndex),
                Tag.ImageIndex.Data = int32(ImageIndex);
            end
        end
        
        % OrdinalNumber (READ-WRITE, DEPENDENT)
        % WITec software requires unique Data/DataClassName indices that
        % begin from 0 and end with NumberOfData-1. Not obeying this will
        % lead to missing data or crash. The ordering does not matter, but
        % will be automatically relabeled in the ascending order by
        % WITec software.
        function OrdinalNumber = get.OrdinalNumber(obj),
            OrdinalNumber = [];
            if ~isempty(obj.TreeDataPair),
                OrdinalNumber = sscanf(obj.TreeDataPair(1).Name, '%*s %d');
                if OrdinalNumber ~= sscanf(obj.TreeDataPair(2).Name, '%*s %d'),
                    OrdinalNumber = []; % Inconsistent ordinal numbering!
                end
            end
        end
        function set.OrdinalNumber(obj, OrdinalNumber),
            if ~isempty(obj.TreeDataPair),
                obj.TreeDataPair(1).Name = sprintf('DataClassName %d', OrdinalNumber);
                obj.TreeDataPair(2).Name = sprintf('Data %d', OrdinalNumber);
            end
        end
        
        % SubType (READ-WRITE, DEPENDENT)
        function SubType = get.SubType(obj),
            SubType = obj.wid_SubType_get();
        end
        function set.SubType(obj, SubType),
            obj.wid_SubType_set(SubType);
        end
        
        % LinksToOthers (READ-ONLY, DEPENDENT)
        function LinksToOthers = get.LinksToOthers(obj),
            % Struct of linked wid-classes
            LinksToOthers = struct.empty;
            if ~isempty(obj.TreeDataPair),
                Tag_Id = obj.TreeDataPair(2).regexp('^[^<]+ID(<[^<]*)*$'); % Should not match with ID under TData!
                strs = WITio.fun.indep.get_valid_and_unique_names({Tag_Id.Name}); % Convert all wit Names to struct-compatible versions
                for ii = 1:numel(Tag_Id),
                    if Tag_Id(ii).Data ~= 0, % Ignore if zero
                        LinksToOthers(1).(strs{ii}) = obj.Project.find_Data(Tag_Id(ii).Data);
                    end
                end
            end
        end
        
        % AllLinksToOthers (READ-ONLY, DEPENDENT)
        function AllLinksToOthers = get.AllLinksToOthers(obj),
            % Same as LinksToOthers but includes also the LinksToOthers of LinksToOthers and so on.
            NewLinksToOthers = obj.LinksToOthers;
            N = fieldnames(NewLinksToOthers);
            C = struct2cell(NewLinksToOthers);
            ii = 1;
            while ii <= numel(C),
                if isempty(C{ii}), NewLinksToOthers = struct();
                else, NewLinksToOthers = C{ii}.LinksToOthers; end
                NewN = cellfun(@(x) [N{ii} '_' x], fieldnames(NewLinksToOthers), 'UniformOutput', false);
                NewC = struct2cell(NewLinksToOthers);
                if ~isempty(NewN), N = [N; NewN]; end
                if ~isempty(NewC), C = [C; NewC]; end
                ii = ii + 1;
            end
            AllLinksToOthers = cell2struct(C, N, 1);
        end
        
        % LinksToThis (READ-ONLY, DEPENDENT)
        function LinksToThis = get.LinksToThis(obj),
            % Array of linked wid-classes
            linked_tags = WITio.obj.wid.find_linked_wits_to_this_wid(obj);
            owner_ids = WITio.obj.wid.find_owner_id_to_this_wit(linked_tags);
            LinksToThis = obj.Project.find_Data(owner_ids);
        end
        
        % AllLinksToThis (READ-ONLY, DEPENDENT)
        function AllLinksToThis = get.AllLinksToThis(obj),
            % Same as LinksToThis but includes also the LinksToThis of LinksToThis and so on.
            AllLinksToThis = WITio.obj.wid.empty;
            if ~isempty(obj.TreeDataPair),
                % First get the object's wit-tree parent tag
                tags = [obj.TreeDataPair(2).Parent WITio.obj.wit.empty];
                % List all the project's ID-tags (except NextDataID and
                % ID<TData) under the Data tree tag
                tags = tags.regexp('^(?!NextDataID)([^<]+ID(List)?(<[^<]*)*(<Data(<WITec (Project|Data))?)?$)');
                ids = obj.Id;
                ii = 1;
                while ii <= numel(ids),
                    % Keep only those ID-tags, which point to this object
                    subtags = tags.match_by_Data_criteria(@(x) any(x == ids(ii)));
                    % Get their owner wid-objects' IDs
                    subids = WITio.obj.wid.find_owner_id_to_this_wit(subtags);
                    % Detect duplicates (to avoid circular loops) and
                    % append without them
                    B_duplicates = any(bsxfun(@eq, ids, subids(:)), 2);
                    ids = [ids subids(~B_duplicates)];
                    % Proceed to next id
                    ii = ii + 1;
                end
                % Exclude this object
                ids = ids(2:end);
                % Get the wid-objects of the tags
                AllLinksToThis = obj.Project.find_Data(ids);
            end
        end

        % Tag (READ-ONLY, DEPENDENT)
        function Tag = get.Tag(obj),
            TreeDataPair = obj.TreeDataPair;
            Tag = struct.empty;
            if ~isempty(TreeDataPair),
                Tree = TreeDataPair(1).Root;
                Tag = struct();
                Tag.Root = Tree;
                Tag.RootVersion = Tree.search_children('Version');
                Tag.Parent = TreeDataPair(1).Parent;
                Tag.DataClassName = TreeDataPair(1);
                Tag.Data = TreeDataPair(2);
                [Tag.Caption, Tag.Id, Tag.ImageIndex] = TreeDataPair(2).search_children('TData').search_children('Caption', 'ID', 'ImageIndex');
            end
        end
        
        
        
        %% OTHER PUBLIC METHODS
        % Object plotting
        h = plot(obj, varargin);
        h_position = plot_position(obj, FigAxNeither, varargin); % To show position of other objects on obj
        h_scalebar = plot_scalebar(obj, FigAxNeither, varargin);
        varargout = manager(obj, varargin); % A wrapper method that shows the given objects via Project Manager view.
        
        % Object copying, destroying, writing
        new = copy(obj); % Copy-method
        varargout = copy_Others_if_shared_and_unshare(obj, varargin); % Copy given shared linked objects and relink
        copy_LinksToOthers(obj); % Copy linked objects (i.e. transformations and interpretations) and relink
        destroy_LinksToOthers(obj); % Destroy links to objects (i.e. transformations and interpretations)
        write(obj, File); % Ability to write selected objects to *.WID-format
        
        % Merge multiple object Data or Graph together (if possible)
        Data_merged = merge_Data(obj, dim);
        Info_Graph_merged = merge_Info_Graph(obj);
        
        % Get object Html-name, which includes the data type icon
        HtmlName = get_HtmlName(obj, isWorkspaceOptimized);
        
        % Reduce object Data
        [Data_cropped, Graph_cropped] = crop_Graph(obj, ind_range, Data_cropped, Graph_cropped);
        [obj, Data_cropped, X_cropped, Y_cropped, Graph_cropped, Z_cropped] = crop(obj, ind_X_begin, ind_X_end, ind_Y_begin, ind_Y_end, ind_Graph_begin, ind_Graph_end, ind_Z_begin, ind_Z_end, isDataCropped);
        [obj, Data_range, Graph_range, Data_range_bg] = filter_bg(obj, varargin);
        
        % Filter object Data
        % If wip_AutoCreateObj == false, then isempty(new_obj) == true.
        [new_obj, varargout] = filter_fun(obj, fun, str_fun, varargin); % Generic (but not yet for 4-D TDGraph!)
        [new_obj, Sum] = filter_sum(obj, varargin);
        [new_obj, Min] = filter_min(obj, varargin);
        [new_obj, Max] = filter_max(obj, varargin);
        [new_obj, CoM] = filter_center_of_mass(obj, varargin);
        [new_obj, I, Pos, Fwhm, I0, R2, Residuals, Fit] = filter_lorentzian(obj, varargin); % If varargin{1} is a cell, then it is treated as an input to the fitting algorithm!
        [new_obj, I, Pos, Fwhm, I0, R2, Residuals, Fit] = filter_gaussian(obj, varargin); % If varargin{1} is a cell, then it is treated as an input to the fitting algorithm!
        [new_obj, I, Pos, Fwhm_L, I0, Fwhm_G, R2, Residuals, Fit] = filter_voigtian(obj, varargin); % If varargin{1} is a cell, then it is treated as an input to the fitting algorithm!
        
        % Spatial filter object Data
        [obj, Average] = spatial_average(obj);
        [obj, Fun] = spatial_fun(obj, fun, str_fun, varargin);
        
        % Spectral stitching
        [new_obj, Graph, Data, W, D] = spectral_stitch(obj, varargin); % Add '-debug' as input to see debug plots
        
        % Unpattern Video Stitching images
        [obj, N_bests, Datas] = unpattern_video_stitching(obj, varargin); % Add '-debug' as input to see debug plots
        
        % Masking tools
        [obj, Data_NaN_masked] = image_mask(obj, varargin);
        [new_obj, image_mask] = image_mask_editor(obj, image_mask);
        
        % Histogram tools
        [new_obj, Bin_Counts, Bin_Centers] = histogram(obj, N_bins, lower_quantile, upper_quantile, range_scaling);
        
        % Interpret various coordinates
        Graph = interpret_Graph(obj, Unit_new, Graph);
        X = interpret_X(obj, Unit_new, X);
        Y = interpret_Y(obj, Unit_new, Y);
        Z = interpret_Z(obj, Unit_new, Z);
    end
    
    %% STATIC PUBLIC METHODS
    methods (Static)
        % File reader
        varargout = read(varargin); % Simple wrapper for WITio.obj.wip.read
        
        % Constructor WID-formatted WIT-tree
        O_wit = new(Version); % WITec Data WIT-tree
        
        % Constructors for various types of objects
        obj = new_Bitmap(O_wit);
        obj = new_Graph(O_wit);
        obj = new_Image(O_wit);
        obj = new_Interpretation_Frequency(O_wit);
        obj = new_Interpretation_InverseSpace(O_wit);
        obj = new_Interpretation_Phase(O_wit);
        obj = new_Interpretation_Space(O_wit);
        obj = new_Interpretation_Spectral(O_wit);
        obj = new_Interpretation_Time(O_wit);
        obj = new_Interpretation_Z(O_wit);
        obj = new_Text(O_wit);
        obj = new_Transformation_InverseSpace(O_wit);
        obj = new_Transformation_Linear(O_wit);
        obj = new_Transformation_LUT(O_wit, LUTSize);
        obj = new_Transformation_Space(O_wit);
        obj = new_Transformation_Spectral(O_wit);
        
        % Other wit-tree related helper functions
        Ids = find_owner_id_to_this_wit(O_wit);
        O_wit = find_linked_wits_to_this_wid(obj);
        
        % DataTree formats
        format = DataTree_format_TData(Version_or_obj);
        
        format = DataTree_format_TDInterpretation(Version_or_obj);
        format = DataTree_format_TDFrequencyInterpretation(Version_or_obj);
        format = DataTree_format_TDInverseSpaceInterpretation(Version_or_obj);
        format = DataTree_format_TDPhaseInterpretation(Version_or_obj);
        format = DataTree_format_TDSpaceInterpretation(Version_or_obj);
        format = DataTree_format_TDSpectralInterpretation(Version_or_obj);
        format = DataTree_format_TDTimeInterpretation(Version_or_obj);
        format = DataTree_format_TDZInterpretation(Version_or_obj);
        
        format = DataTree_format_TDTransformation(Version_or_obj);
        format = DataTree_format_TDLinearTransformation(Version_or_obj);
        format = DataTree_format_TDLUTTransformation(Version_or_obj);
        format = DataTree_format_TDSpaceTransformation(Version_or_obj);
        format = DataTree_format_TDSpectralTransformation(Version_or_obj);
        
        %% OTHER PUBLIC METHODS
        [Data_range, Graph_range, Data_range_bg, range] = crop_Graph_with_bg_helper(Data, Graph, range, bg_avg_lower, bg_avg_upper);
        h_image = plot_position_Image_helper(Ax, positions, color);
        h_line = plot_position_Line_helper(Ax, positions, color);
        h_point = plot_position_Point_helper(Ax, positions, color);
        h = plot_scalebar_helper(Ax, image_size, image_size_in_SU, image_SU, varargin);
        [Graph, Data, W, D] = spectral_stitch_helper(Graphs_nm, Datas, isdebug);
        [I_best, N_best, cropIndices] = unpattern_video_stitching_helper(I, N_SI_XY, varargin); % Add '-debug' as input to see debug plots
    end
    
    %% PRIVATE METHODS
    methods (Access = private)
        Data = wid_Data_get_LineValid(obj, Data);
        
        Data = wid_Data_get_DataType(obj, Data);
        Data = wid_Data_set_DataType(obj, Data);
        
        out = wid_DataTree_get(obj, varargin); % For (un)formatted structs
        wid_DataTree_set(obj, in, varargin); % For (un)formatted structs
        
        out = wid_Data_get(obj);
        out = wid_Data_get_Bitmap(obj);
        out = wid_Data_get_Graph(obj);
        out = wid_Data_get_Image(obj);
        out = wid_Data_get_Text(obj);
        
        wid_Data_set(obj, in);
        wid_Data_set_Bitmap(obj, in);
        wid_Data_set_Graph(obj, in);
        wid_Data_set_Image(obj, in);
        wid_Data_set_Text(obj, in);
        
        out = wid_Info_get(obj); % Returns struct for TDBitmap, TDGraph and TDImage
        
        out = wid_SubType_get(obj);
        wid_SubType_set(obj, in);
    end
end
