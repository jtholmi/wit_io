% MIT No Attribution License (https://opensource.org/license/mit-0)
% 
% Copyright (c) 2019 Joonas T. Holmi (jtholmi@gmail.com), 2019-2021 Aalto University

% Make hard copy of the wid and all its linked objects and append to wip.
function new = copy(obj),
    new = WITio.obj.wid(size(obj)); % Return empty if no obj given
    for ii = 1:numel(obj),
        % CASE: deleted
        if ~obj(ii).isvalid,
            delete(new(ii)); % Delete copy as well
            continue;
        end
        % CASE: dummy
        if isempty(obj(ii).Project),
            % Dummy copy as well
            continue;
        end
        % OTHERWISE:
        % Append the tag copies to the root (ENCLOSED BY {} TO AVOID TOUCHING THE LINKED IDS)
        WITio.obj.wip.append(obj(ii).TreeDataPair(2).Root, {obj(ii).TreeDataPair});
        % Last index of Project Data has our new copy, because append calls wid-constructor via wip_update_Data-event
        new(ii) = obj(ii).Project.Data(end);
        % Copy the linked objects AFTER the tags have been copied!
        new(ii).copy_LinksToOthers();
        % These were AUTOMATICALLY added to the wip Project object!
    end
end
