% MIT No Attribution License (https://opensource.org/license/mit-0)
% 
% Copyright (c) 2019 Joonas T. Holmi (jtholmi@gmail.com), 2019-2021 Aalto University

function format = wid_Data_format_TDFrequencyInterpretation(obj),
    % Each row: wit-tag name, {subformat}
    format_TDFrequencyInterpretation = ...
        [obj.wid_Data_format_TData(); ...
        obj.wid_Data_format_TDInterpretation()];
    
    format = format_TDFrequencyInterpretation;
end
