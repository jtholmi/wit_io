% MIT No Attribution License (https://opensource.org/license/mit-0)
% 
% Copyright (c) 2019 Joonas T. Holmi (jtholmi@gmail.com), 2019-2021 Aalto University

function [new_obj, Sum] = filter_sum(obj, varargin),
    fun = @(I, X, dim) sum(I, dim);
    str_fun = 'Sum';
    [new_obj, Sum] = obj.filter_fun(fun, str_fun, varargin{:});
end
