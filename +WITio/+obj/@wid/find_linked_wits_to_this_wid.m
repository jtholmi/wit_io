% MIT No Attribution License (https://opensource.org/license/mit-0)
% 
% Copyright (c) 2019 Joonas T. Holmi (jtholmi@gmail.com), 2019-2021 Aalto University

% This returns the linked wit-objects pointing directly to the provided
% wid-objects. This returns a cell array if obj is an array.
function O_wit = find_linked_wits_to_this_wid(obj), %#ok
    O_wit = WITio.obj.wit.empty;
    for ii = 1:numel(obj), %#ok
        if ~isempty(obj(ii).TreeDataPair), %#ok
            % Get the parent tag of the wid-object's wit-tree branches
            tags = [obj(ii).TreeDataPair(2).Parent WITio.obj.wit.empty];
            % List all the project's ID-tags (except NextDataID and
            % ID<TData) under the Data-tag
            tags = tags.regexp_all_Names('^(?!NextDataID).+ID(List)?$');
            % Keep only those ID-tags that point directly to this object
            O_wit = [O_wit tags.match_by_Data_criteria(@(x) any(x == obj.Id))];
        end
    end
end
