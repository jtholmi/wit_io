% MIT No Attribution License (https://opensource.org/license/mit-0)
% 
% Copyright (c) 2019 Joonas T. Holmi (jtholmi@gmail.com), 2019-2021 Aalto University

function destroy_LinksToOthers(obj),
    % Handle transformations and interpretations
    Tag_Id = obj.TreeDataPair(2).regexp('^[^<]+ID(<[^<]*)*$'); % Should not match with ID under TData!
    for ii = 1:numel(Tag_Id),
        Tag_Id(ii).Data = int32(0); % Must be int32!
    end
end
