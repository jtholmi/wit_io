% MIT No Attribution License (https://opensource.org/license/mit-0)
% 
% Copyright (c) 2019 Joonas T. Holmi (jtholmi@gmail.com), 2019-2021 Aalto University

function [new_obj, CoM] = filter_center_of_mass(obj, varargin),
    fun = @(I, X, dim) sum(bsxfun(@times, I, X), dim)./sum(I, dim);
    str_fun = 'Center of Mass';
    [new_obj, CoM] = obj.filter_fun(fun, str_fun, varargin{:});
end
