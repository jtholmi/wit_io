% MIT No Attribution License (https://opensource.org/license/mit-0)
% 
% Copyright (c) 2019 Joonas T. Holmi (jtholmi@gmail.com), 2019-2021 Aalto University

% A wrapper method that shows the given objects via Project Manager view.
function varargout = manager(obj, varargin),
    O_wip = unique([obj.Project]); % Get Projects
    [varargout{1:nargout}] = O_wip.manager('-Data', obj, '-all', '-indices', '-nosort', varargin{:});
end
