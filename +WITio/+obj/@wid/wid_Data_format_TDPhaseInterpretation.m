% MIT No Attribution License (https://opensource.org/license/mit-0)
% 
% Copyright (c) 2019 Joonas T. Holmi (jtholmi@gmail.com), 2019-2021 Aalto University

function format = wid_Data_format_TDPhaseInterpretation(obj),
    % Each row: wit-tag name, {subformat}
    format_TDPhaseInterpretation = ...
        [obj.wid_Data_format_TData(); ...
        obj.wid_Data_format_TDInterpretation()];
    
    format = format_TDPhaseInterpretation;
end
