% MIT No Attribution License (https://opensource.org/license/mit-0)
% 
% Copyright (c) 2019 Joonas T. Holmi (jtholmi@gmail.com), 2019-2021 Aalto University

% Helper function to GET (un)formatted struct-content to wit-tree.
function out = wid_DataTree_get(obj, varargin),
    out = WITio.obj.wit.DataTree_get(obj.TreeDataPair(2), varargin{:});
end
