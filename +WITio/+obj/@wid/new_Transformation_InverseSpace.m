% MIT No Attribution License (https://opensource.org/license/mit-0)
% 
% Copyright (c) 2019 Joonas T. Holmi (jtholmi@gmail.com), 2019-2021 Aalto University

function obj = new_Transformation_InverseSpace(O_wit),
    if nargin == 0 || isempty(O_wit), O_wit = WITio.obj.wid.new(); end % Create O_wit
    obj = WITio.obj.wid.new_Transformation_Space(O_wit);
    obj.Data.TDTransformation.UnitKind = int32(6); % 1 = Space (X, Y, Z), 6 = InverseSpace (Px, Py, Px) % Required for position cursor in WITec software
end
