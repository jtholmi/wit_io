% MIT No Attribution License (https://opensource.org/license/mit-0)
% 
% Copyright (c) 2019 Joonas T. Holmi (jtholmi@gmail.com), 2019-2021 Aalto University
% 
% Use this if you want direct READ+WRITE access to Name-Value
% pairs of each tag. This is particularly useful for quick
% modifications of small wit-formatted files.
classdef debug < dynamicprops, % Since R2008a
    methods
        % Use this constructor for reverse engineering to achieve interoperability.
        function obj = debug(O_wit),
            if nargin > 0,
                obj.collapse(O_wit);
            end
        end
        
        % This uses different property-naming than collapse-function under
        % wit-class because of automatic property sorting feature. Anyhow
        % the output is more compacto for this class due to lack of Tags.
        function collapse(obj, O_wit, Pre),
            if nargin < 3, Pre = 'Tree_'; end
            for ii = 1:numel(O_wit),
                Id = sprintf(sprintf('%%0%dd', floor(log10(numel(O_wit))+1)), ii);
                prop_name = addprop(obj, [Pre Id '_Name']);
                prop_name.GetMethod = @(x) get_Name(x, O_wit(ii));
                prop_name.SetMethod = @(x, y) set_Name(x, y, O_wit(ii));
                if isa(O_wit(ii).Data, 'WITio.obj.wit'),
                    obj.collapse(O_wit(ii).Data, [Pre Id '_Value_']);
                else,
                    prop_data = addprop(obj, [Pre Id '_Value']);
                    prop_data.GetMethod = @(x) get_Data(x, O_wit(ii));
                    prop_data.SetMethod = @(x, y) set_Data(x, y, O_wit(ii));
                end
            end
        end
        
        function Name = get_Name(~, Tag),
            Name = Tag.Name;
        end
        
        function set_Name(~, Name, Tag),
            Tag.Name = Name;
        end
        
        function Data = get_Data(~, Tag),
            Data = Tag.Data;
        end
        
        function set_Data(~, Data, Tag),
            Tag.Data = Data;
        end
    end
end
