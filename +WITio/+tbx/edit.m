% MIT No Attribution License (https://opensource.org/license/mit-0)
% 
% Copyright (c) 2019 Joonas T. Holmi (jtholmi@gmail.com), 2019-2021 Aalto University

% This function is intended replace use of edit within WITio toolbox,
% because it allows non-interactive mode to kick in on demand.
function edit(file),
    if nargin == 0,
        S = dbstack('-completenames'); % Find out what function called this
        if numel(S) < 2, return; end % Stop if nothing to find
        file = S(2).file;
    end
    if ~WITio.tbx.pref.get('AutoStopEdit', false),
        edit(file);
    end
end
