% MIT No Attribution License (https://opensource.org/license/mit-0)
% 
% Copyright (c) 2019 Joonas T. Holmi (jtholmi@gmail.com), 2019-2021 Aalto University

% This function returns the WITio toolbox folder.
function path = path(),
    path = fileparts([mfilename('fullpath') '.m']);
    path = regexprep(path, '([\\\/]+\+[^\\\/]*)+$', ''); % Step back the package '+'-prefixed folders
end
