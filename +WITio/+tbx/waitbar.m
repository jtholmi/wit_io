% MIT No Attribution License (https://opensource.org/license/mit-0)
% 
% Copyright (c) 2019 Joonas T. Holmi (jtholmi@gmail.com), 2019-2021 Aalto University

% This wraps built-in waitbar but gives no waitbar if -nodesktop.
function varargout = waitbar(varargin),
    % Determine whether or not to show waitbar
    isDesktop = usejava('desktop'); % Test if MATLAB is running in Desktop-mode
    if isDesktop, [varargout{1:nargout}] = waitbar(varargin{:});
    else, [varargout{1:nargout}] = deal([]); end
end
