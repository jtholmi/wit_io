% MIT No Attribution License (https://opensource.org/license/mit-0)
% 
% Copyright (c) 2019 Joonas T. Holmi (jtholmi@gmail.com), 2019-2021 Aalto University

% Tests if WITio toolbox preferences exist using built-in ISPREF function.
% This behaves like ISPREF but also treats the struct fieldnames as 'pref'.
function tf = is(pref),
    if nargin == 0, % SPECIAL CASE: no input
        tf = true;
        return;
    elseif isstruct(pref), % SPECIAL CASE: a struct input
        pref = fieldnames(pref);
    end
    tf = ispref('WITio', pref);
end
