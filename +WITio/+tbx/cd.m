% MIT No Attribution License (https://opensource.org/license/mit-0)
% 
% Copyright (c) 2019 Joonas T. Holmi (jtholmi@gmail.com), 2019-2021 Aalto University

% Gets to the toolbox main folder
function cd(),
    WITio_folder = WITio.tbx.path;
    fprintf('Changing current folder to the main folder of the WITio toolbox:\n%s\n', WITio_folder);
    
    % Change folder to the toolbox main folder
    cd(WITio_folder);
end
