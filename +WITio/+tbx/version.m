% MIT No Attribution License (https://opensource.org/license/mit-0)
% 
% Copyright (c) 2019 Joonas T. Holmi (jtholmi@gmail.com), 2019-2021 Aalto University

% This function returns the WITio toolbox version.
function version = version(WITio_folder),
    if nargin == 0, WITio_folder = WITio.tbx.path; end
    % Read from file
    file_v = fullfile(WITio_folder, 'Contents.m');
    fid_v = fopen(file_v, 'r', 'n', 'UTF-8');
    if fid_v == -1, error('Cannot open ''%s'' for reading.', file_v); end
    ocu_v = onCleanup(@() fclose(fid_v)); % Ensure file stream closes on error
    data = reshape(fread(fid_v, inf, 'char=>char'), 1, []);
    version = regexprep(data, '^.*Version\s(\d+(\.\d+)*)\s.*$', '$1');
    clear ocu_v
end
