% MIT No Attribution License (https://opensource.org/license/mit-0)
% 
% Copyright (c) 2019 Joonas T. Holmi (jtholmi@gmail.com), 2019-2021 Aalto University

% This always returns a 'MIT No Attribution License' char array. If allowed by
% the user preference, then open a License Dialog -window, describing the
% WITio toolbox license's span and freedom of usage. To reset this user
% preference, execute WITio.tbx.pref.rm('license_dialog').
function license = license(),
    % Return WITio's license
    license = 'MIT No Attribution License';
    
    % Test if non-interactive mode
    AutoCloseInSeconds = WITio.tbx.pref.get('AutoCloseInSeconds', Inf);
    if ~isinf(AutoCloseInSeconds) && AutoCloseInSeconds >= 0, return; end

    % Convert non-false-value to 'ask' (required by uigetpref)
    value = WITio.tbx.pref.get('license_dialog');
    if ~islogical(value) || numel(value) ~= 1 || value ~= false,
        WITio.tbx.pref.set('license_dialog', 'ask');
    end
    
    % Show dialog (if user preference allows it)
    uigetpref('WITio', 'license_dialog', 'License Dialog', ...
    {'Code of ''WITio'' toolbox is mostly open-sourced under', ...
    'free and permissive public-domain-equivalent', ...
    'MIT No Attribution License (https://opensource.org/license/mit-0)!', ...
    '', ...
    'Toolbox users are welcome to contribute to the code at', ...
    'https://gitlab.com/jtholmi/wit_io.', ...
    '', ...
    '(Only exceptions to this license can be found among', ...
    'the ''third party''-folder code under ''WITio.tbx.path''.)'}, ...
    'OK');
    
    % Convert uigetpref's 'ask'-value to true and otherwise false.
    if strcmp(WITio.tbx.pref.get('license_dialog'), 'ask'),
        WITio.tbx.pref.set('license_dialog', true);
    else,
        WITio.tbx.pref.set('license_dialog', false);
    end
end
