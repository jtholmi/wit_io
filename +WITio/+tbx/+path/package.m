% MIT No Attribution License (https://opensource.org/license/mit-0)
% 
% Copyright (c) 2019 Joonas T. Holmi (jtholmi@gmail.com), 2019-2021 Aalto University

% This function return the WITio package folder.
function path = package(),
    path = fullfile(WITio.tbx.path, '+WITio'); % Step into the main package folder
end
