% MIT No Attribution License (https://opensource.org/license/mit-0)
% 
% Copyright (c) 2019 Joonas T. Holmi (jtholmi@gmail.com), 2019-2021 Aalto University

% This function return the WITio.doc package folder.
function path = doc(),
    path = fullfile(WITio.tbx.path.package, '+doc');
end
