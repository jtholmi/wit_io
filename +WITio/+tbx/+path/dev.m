% MIT No Attribution License (https://opensource.org/license/mit-0)
% 
% Copyright (c) 2019 Joonas T. Holmi (jtholmi@gmail.com), 2019-2021 Aalto University

% This function return the WITio.dev package folder.
function path = dev(),
    path = fullfile(WITio.tbx.path.package, '+dev');
end
