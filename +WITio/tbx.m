% MIT No Attribution License (https://opensource.org/license/mit-0)
% 
% Copyright (c) 2019 Joonas T. Holmi (jtholmi@gmail.com), 2019-2021 Aalto University

% Display the content of obj-package
function tbx(),
    WITio.fun.href_dir(WITio.tbx.path.tbx);
end
