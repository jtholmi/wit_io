% MIT No Attribution License (https://opensource.org/license/mit-0)
% 
% Copyright (c) 2019 Joonas T. Holmi (jtholmi@gmail.com), 2019-2021 Aalto University

function gitlab(),
    % Open the toolbox's main url
    url = 'https://gitlab.com/jtholmi/wit_io';
    fprintf('Opening the main page of the WITio toolbox at GitLab:\n%s\n', url);
    web(url, '-browser');
end
