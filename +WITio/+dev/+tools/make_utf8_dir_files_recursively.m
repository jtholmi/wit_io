% MIT No Attribution License (https://opensource.org/license/mit-0)
% 
% Copyright (c) 2019 Joonas T. Holmi (jtholmi@gmail.com), 2019-2021 Aalto University

function files_made_utf8 = make_utf8_dir_files_recursively(folder, text_extensions, encoding),
    if nargin < 3, encoding = ''; end
    if nargin < 2 || isempty(text_extensions), text_extensions = {'.m', '.txt'}; end
    if nargin < 1, folder = ''; end
    files = WITio.dev.tools.get_dir_files_recursively(folder);
    [~, ~, exts] = cellfun(@fileparts, files, 'UniformOutput', false); % Backward compatible with R2011a
    files_made_utf8 = {};
    for ii = 1:numel(exts),
        if any(strcmp(exts{ii}, text_extensions)),
            fid = fopen(files{ii}, 'rb');
            if fid == -1, error('Cannot open ''%s'' for writing.', files{ii}); end
            ocu = onCleanup(@() fclose(fid)); % Ensure file stream closes on error
            A = fread(fid, 'uint8=>uint8');
            clear ocu;
            B = native2unicode(A, encoding);
            C = unicode2native(B, 'UTF-8');
            if numel(A) ~= numel(C) || any(A ~= C),
                fid = fopen(files{ii}, 'wb');
                if fid == -1, error('Cannot open ''%s'' for writing.', files{ii}); end
                ocu = onCleanup(@() fclose(fid)); % Ensure file stream closes on error
                fwrite(fid, C);
                clear ocu;
                files_made_utf8{end+1} = files{ii};
            end
        end
    end
end
