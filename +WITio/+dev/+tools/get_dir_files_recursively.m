% MIT No Attribution License (https://opensource.org/license/mit-0)
% 
% Copyright (c) 2019 Joonas T. Holmi (jtholmi@gmail.com), 2019-2021 Aalto University

function [files, datenums, bytes] = get_dir_files_recursively(folder),
    [files, datenums, bytes] = deal({});
    if nargin == 0 || isempty(folder), % If no folder is provided, then ask for it
        % Select folder in which to begin recursive search of files
        folder = uigetdir(WITio.tbx.pref.get('latest_folder', cd));
        if folder == 0, return; end % Abort as no folder was selected!
        WITio.tbx.pref.set('latest_folder', folder);
    end
    S = dir(folder);
    S_files = S(~[S.isdir]);
    files = cellfun(@(s) fullfile(folder, s), reshape({S_files.name}, [], 1), 'UniformOutput', false); % Backward compatible with R2011a
    datenums = reshape([S_files.datenum], [], 1);
    bytes = reshape([S_files.bytes], [], 1);
    folders = {S([S.isdir]).name};
    for ii = numel(folders):-1:1,
        if ~strcmp(folders{ii}, '.') && ~strcmp(folders{ii}, '..'), % Skip . and ..
            [subfiles, subdatenums, subbytes] = WITio.dev.tools.get_dir_files_recursively(fullfile(folder, folders{ii}));
            files = [subfiles; files];
            datenums = [subdatenums; datenums];
            bytes = [subbytes; bytes];
        end
    end
end
