% MIT No Attribution License (https://opensource.org/license/mit-0)
% 
% Copyright (c) 2019 Joonas T. Holmi (jtholmi@gmail.com), 2019-2021 Aalto University

% This helper function is called before the next release and it updates all
% the necessary files with the new version number. For instance, if old
% version is 1.2.3 and new version is 1.3.0, then provide '1.2.3' and
% '1.3.0'.

function update_toolbox_version(new_version),
    old_version = WITio.tbx.version;

    % Validate input
    if nargin == 0 || ~ischar(new_version),
        error('Accepting only char array inputs, i.e., ''%s'' (current version)!', old_version);
    end
    
    % Do nothing if same version
    if strcmp(old_version, new_version), return; end
    
    % Get the toolbox folder
    stored_cd = cd;
    cd_onCleanup = onCleanup(@() cd(stored_cd)); % Return to original folder upon exit or error
    cd(WITio.tbx.path); % Go to the toolbox folder
    
    %% UPDATE Contents.m
    file = 'Contents.m';
    
    % Read from file
    fid = fopen(file, 'r', 'n', 'UTF-8');
    if fid == -1, error('Cannot open ''%s'' for reading.', file); end
    ocu = onCleanup(@() fclose(fid)); % Ensure file stream closes on error
    data = reshape(fread(fid, inf, 'char=>char'), 1, []);
    clear ocu;
    
    % Update file
    data = strrep(data, ['Version ' old_version ' ('], ['Version ' new_version ' (']);
    
    % Write to file
    fid = fopen(file, 'w', 'n', 'UTF-8');
    if fid == -1, error('Cannot open ''%s'' for writing.', file); end
    ocu = onCleanup(@() fclose(fid)); % Ensure file stream closes on error
    fwrite(fid, data, 'char');
    clear ocu;
    
    %% UPDATE CHANGELOG.md
    file = 'CHANGELOG.md';
    
    % Read from file
    fid = fopen(file, 'r', 'n', 'UTF-8');
    if fid == -1, error('Cannot open ''%s'' for reading.', file); end
    ocu = onCleanup(@() fclose(fid)); % Ensure file stream closes on error
    data = reshape(fread(fid, inf, 'char=>char'), 1, []);
    clear ocu;
    
    % Update file
    try,
        datestrnow = char(datetime('now', 'Format', 'yyyy-MM-dd'));
    catch,
        datestrnow = datestr(now, 'yyyy-mm-dd');
    end
    data = strrep(data, sprintf('## [Unreleased]\r\n'), sprintf('## [Unreleased]\r\n\r\n\r\n\r\n## [%s] - %s\r\n', new_version, datestrnow));
    data = strrep(data, sprintf('[master,'), sprintf('[%s,', new_version)); % Update possible master-branch [Unreleased] link references
    data = strrep(data, sprintf('https://gitlab.com/jtholmi/wit_io/-/blob/master/'), sprintf('https://gitlab.com/jtholmi/wit_io/-/blob/v%s/', new_version)); % Update possible master-branch [Unreleased] link targets
    data = strrep(data, ['[Unreleased]: https://gitlab.com/jtholmi/wit_io/-/compare/v' old_version '...master'], ['[Unreleased]: https://gitlab.com/jtholmi/wit_io/-/compare/v' new_version '...master' sprintf('\r\n') '[' new_version ']: https://gitlab.com/jtholmi/wit_io/-/compare/v' old_version '...v' new_version]);
    
    % Write to file
    fid = fopen(file, 'w', 'n', 'UTF-8');
    if fid == -1, error('Cannot open ''%s'' for writing.', file); end
    ocu = onCleanup(@() fclose(fid)); % Ensure file stream closes on error
    fwrite(fid, data, 'char');
    clear ocu;
    
    %% UPDATE README.md
    file = 'README.md';
    
    % Read from file
    fid = fopen(file, 'r', 'n', 'UTF-8');
    if fid == -1, error('Cannot open ''%s'' for reading.', file); end
    ocu = onCleanup(@() fclose(fid)); % Ensure file stream closes on error
    data = reshape(fread(fid, inf, 'char=>char'), 1, []);
    clear ocu;
    
    % Update file
    data = strrep(data, ['WITio v' old_version ' Changelog Badge'], ['WITio v' new_version ' Changelog Badge']);
    data = strrep(data, ['https://img.shields.io/badge/changelog-WITio_v' old_version '-0000ff.svg'], ['https://img.shields.io/badge/changelog-WITio_v' new_version '-0000ff.svg']);
    
    % Write to file
    fid = fopen(file, 'w', 'n', 'UTF-8');
    if fid == -1, error('Cannot open ''%s'' for writing.', file); end
    ocu = onCleanup(@() fclose(fid)); % Ensure file stream closes on error
    fwrite(fid, data, 'char');
    clear ocu;
end
