% MIT No Attribution License (https://opensource.org/license/mit-0)
% 
% Copyright (c) 2019 Joonas T. Holmi (jtholmi@gmail.com), 2019-2021 Aalto University

% Runs all available tests
function tests(),
    WITio.tbx.pref.set('ifnodesktop_counter', 0); % Reset counter used for file exporting if -nodesktop mode
    WITio.dev.tests.demo;
    WITio.dev.tests.try_catch('dev.tests.wit_bread_bwrite');
    WITio.dev.tests.try_catch('dev.tests.wid_wip_wit_sync');
end
