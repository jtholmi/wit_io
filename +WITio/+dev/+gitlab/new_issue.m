% MIT No Attribution License (https://opensource.org/license/mit-0)
% 
% Copyright (c) 2019 Joonas T. Holmi (jtholmi@gmail.com), 2019-2021 Aalto University

function new_issue(),
    % Open the toolbox's new issue url
    url = 'https://gitlab.com/jtholmi/wit_io/issues/new';
    fprintf('Opening the New Issue page of the WITio toolbox at GitLab:\n%s\n', url);
    web(url, '-browser');
end
