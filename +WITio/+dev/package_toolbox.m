% MIT No Attribution License (https://opensource.org/license/mit-0)
% 
% Copyright (c) 2019 Joonas T. Holmi (jtholmi@gmail.com), 2019-2021 Aalto University

% Use this function to generate new backward-compatible toolbox installer without R2014b!
function package_toolbox(WITio_folder),
    if nargin == 0, WITio_folder = WITio.tbx.path; end
    WITio_appname = 'WITio';
    WITio_authnamewatermark = 'Joonas T. Holmi';
    WITio_email = 'jtholmi@gmail.com';
    WITio_company = 'Independent Researcher, Evijärvi, Finland; Aalto University';
    WITio_summary = 'WITio: A MATLAB data evaluation toolbox to script broader insights into big data from WITec microscopes';
    WITio_screenshot = fullfile(WITio_folder, 'README.png');
    WITio_description = {WITio.fun.indep.mystrjoin({...
        'WITio is intended for the users of WITec.de microscopes, who work with WITec Project/Data (*.wip/*.wid) files (v0-v7) and wish to enrich their data evaluation by customized automation scripts within MATLAB environment.' ...
        'Thanks to its fast bidirectional read/write capabilities, WITio bridges the best of both WITec software and MATLAB software.', ...
        'The toolbox is a great script-based automation platform for big data analysis and post-processing, which reduces a lot of slow error-prone manual work.', ...
        'This includes file batch processing, easily accessible customized tools and more systematic data evaluation.', ...
        'When combined with the WITec software''s Autosaving feature, they can together even achieve powerful real-time during-measurement task automation for industrial applications.', ...
        'To summarize, WITio aims to constantly open up new research and commercial possibilities to the WITec community by jointly keeping up with the ever-increasing big data for broader insights.' ...
        }, ' '), ...
        '', ...
        'Installation and usage: https://gitlab.com/jtholmi/wit_io', ...
        '', ...
        'Full version history: https://gitlab.com/jtholmi/wit_io/CHANGELOG.md', ...
        '', ...
        'Contributing (e.g., bug reports and feature requests): https://gitlab.com/jtholmi/wit_io/issues'};
    
    % Change folder
    old_folder = cd;
    ocu = onCleanup(@() cd(old_folder)); % Return to old folder on exit
    cd(WITio_folder); % Temporarily change folder
    
    %% GET VERSION NUMBER
    WITio_version = WITio.tbx.version(WITio_folder);
    
    %% GENERATE WITio.mltbx
    if exist('WITio_mltbx', 'dir') == 7,
        rmdir('WITio_mltbx', 's');
    end

    % Expected main files and main folders
    expected_files = {'.gitignore', '*.md', '*.m', 'LICENSE', 'README.png', 'WITio.png'};
    expected_folders_package = {'+WITio'};
    expected_folders_other = {'.gitlab', 'third party'};

    % Excluded file extensions
    exclude_exts = {'.sh', '.asv', '.prj'};

    % Create and populate folders
    mkdir('WITio_mltbx');
    mkdir('WITio_mltbx/_rels');
    mkdir('WITio_mltbx/fsroot');
    mkdir('WITio_mltbx/metadata');
    for ii = 1:numel(expected_folders_package),
        copyfile(expected_folders_package{ii}, fullfile('WITio_mltbx/fsroot/', expected_folders_package{ii}));
    end
    for ii = 1:numel(expected_folders_other),
        copyfile(expected_folders_other{ii}, fullfile('WITio_mltbx/fsroot/', expected_folders_other{ii}));
    end
    for ii = 1:numel(expected_files),
        copyfile(expected_files{ii}, 'WITio_mltbx/fsroot');
    end

    % Parse files and their datenums
    encode_unsafe_chars_dir_files_recursively('WITio_mltbx/fsroot');
    % WITio.dev.tools.make_utf8_dir_files_recursively('WITio_mltbx/fsroot', {'.m', '.txt'}, 'windows-1252');
    [files, datenums] = WITio.dev.tools.get_dir_files_recursively('WITio_mltbx/fsroot');
    [~, ~, exts] = cellfun(@fileparts, files, 'UniformOutput', false); % Backward compatible with R2011a
    for ii = 1:numel(exclude_exts),
        B_excluded = strcmp(exts, exclude_exts{ii});
        files_remove = files(B_excluded);
        cellfun(@delete, files_remove);
        files = files(~B_excluded);
        datenums = datenums(~B_excluded);
        exts = exts(~B_excluded);
    end

    try,
        datestrnow = char(datetime('now', 'Format', 'yyyy-MM-dd''T''HH:mm:ss''Z'''));
        datestrs = char(datetime(datenums, 'ConvertFrom', 'datenum', 'Format', 'yyyy-MM-dd''T''HH:mm:ss''Z'''));
    catch,
        datestrnow = datestr(now, 'yyyy-mm-ddTHH:MM:SSZ');
        datestrs = datestr(datenums, 'yyyy-mm-ddTHH:MM:SSZ');
    end
    datestrs = cellstr(datestrs);

    % Write XML files
    file_xml = 'WITio_mltbx/_rels/.rels';
    fid_xml = fopen(file_xml, 'w', 'n', 'UTF-8');
    if fid_xml == -1, error('Cannot open ''%s'' for writing.', file_xml); end
    ocu_xml = onCleanup(@() fclose(fid_xml)); % Ensure file stream closes on error
    fprintf(fid_xml, '<?xml version="1.0" encoding="UTF-8" standalone="yes" ?>');
    fprintf(fid_xml, '<Relationships xmlns="http://schemas.openxmlformats.org/package/2006/relationships">');
    fprintf(fid_xml, '<Relationship Id="rId1" Target="metadata/addonProperties.xml" Type="http://schemas.mathworks.com/matlab/addon/2014/relationships/addonProperties"/>');
    fprintf(fid_xml, '<Relationship Id="rId100" Target="metadata/primaryScreenShot.png" Type="http://schemas.mathworks.com/matlab/addon/2014/relationships/primaryScreenShot"/>');
    fprintf(fid_xml, '<Relationship Id="rId101" Target="metadata/primaryScreenShot.png" Type="http://schemas.openxmlformats.org/package/2006/relationships/metadata/thumbnail"/>');
    fprintf(fid_xml, '<Relationship Id="rId2" Target="metadata/mwcoreProperties.xml" Type="http://schemas.mathworks.com/package/2012/relationships/coreProperties"/>');
    fprintf(fid_xml, '<Relationship Id="rId3" Target="metadata/coreProperties.xml" Type="http://schemas.openxmlformats.org/package/2006/relationships/metadata/core-properties"/>');
    fprintf(fid_xml, '<Relationship Id="rId4" Target="metadata/filesystemManifest.xml" Type="http://schemas.mathworks.com/package/2013/relationships/filesystemManifest"/>');
    fprintf(fid_xml, '<Relationship Id="rId5" Target="metadata/examples.xml" Type="http://schemas.mathworks.com/matlab/addon/2014/relationships/examples"/>');
    fprintf(fid_xml, '<Relationship Id="rId6" Target="metadata/systemRequirements.xml" Type="http://schemas.mathworks.com/matlab/addon/2014/relationships/systemRequirements"/>');
    fprintf(fid_xml, '<Relationship Id="rId7" Target="metadata/applications.xml" Type="http://schemas.mathworks.com/matlab/addon/2014/relationships/applications"/>');
    fprintf(fid_xml, '<Relationship Id="rId8" Target="metadata/configuration.xml" Type="http://schemas.mathworks.com/matlab/addon/2014/relationships/configuration"/>');
    fprintf(fid_xml, '</Relationships>');
    clear ocu_xml;

    file_xml = 'WITio_mltbx/metadata/addonProperties.xml';
    fid_xml = fopen(file_xml, 'w', 'n', 'UTF-8');
    if fid_xml == -1, error('Cannot open ''%s'' for writing.', file_xml); end
    ocu_xml = onCleanup(@() fclose(fid_xml)); % Ensure file stream closes on error
    fprintf(fid_xml, '<?xml version="1.0" encoding="UTF-8" standalone="yes" ?>');
    fprintf(fid_xml, '<addonProperties xmlns="http://schemas.mathworks.com/matlab/addon/2014/addonProperties">');
    fprintf(fid_xml, '<identifier type="GUID">41e961e3-50b7-45ed-b1c3-fb9b2ede765f</identifier>');
    fprintf(fid_xml, '<description>%s</description>', WITio.fun.indep.mystrjoin(WITio_description, sprintf('\n')));
    fprintf(fid_xml, '<authors>');
    fprintf(fid_xml, '<author>');
    fprintf(fid_xml, '<name>%s</name>', WITio_authnamewatermark);
    fprintf(fid_xml, '<contact>%s</contact>', WITio_email);
    fprintf(fid_xml, '<organization>%s</organization>', WITio_company);
    fprintf(fid_xml, '</author>');
    fprintf(fid_xml, '</authors>');
    fprintf(fid_xml, '</addonProperties>');
    clear ocu_xml;

    file_xml = 'WITio_mltbx/metadata/applications.xml';
    fid_xml = fopen(file_xml, 'w', 'n', 'UTF-8');
    if fid_xml == -1, error('Cannot open ''%s'' for writing.', file_xml); end
    ocu_xml = onCleanup(@() fclose(fid_xml)); % Ensure file stream closes on error
    fprintf(fid_xml, '<?xml version="1.0" encoding="UTF-8" standalone="yes" ?>');
    fprintf(fid_xml, '<applications xmlns="http://schemas.mathworks.com/matlab/addon/2014/applications"/>');
    clear ocu_xml;

    file_xml = 'WITio_mltbx/metadata/configuration.xml';
    fid_xml = fopen(file_xml, 'w', 'n', 'UTF-8');
    if fid_xml == -1, error('Cannot open ''%s'' for writing.', file_xml); end
    ocu_xml = onCleanup(@() fclose(fid_xml)); % Ensure file stream closes on error
    fprintf(fid_xml, '<?xml version="1.0" encoding="UTF-8" standalone="yes" ?>');
    fprintf(fid_xml, '<configuration xmlns="http://schemas.mathworks.com/matlab/addon/2014/configuration">');
    fprintf(fid_xml, '<matlabPaths>');
    expected_folders_other = WITio.fun.indep.mystrjoin(cellfun(@genpath, expected_folders_other, 'UniformOutput', false), ''); % Get all relevant subfolders
    expected_folders_other = strrep(expected_folders_other, WITio_folder, ''); % From absolute to relative paths
    expected_folders_other = strrep(expected_folders_other, filesep, '/'); % Convert filesep's to /'s
    expected_folders_other = strrep(expected_folders_other, pathsep, '/</matlabPath><matlabPath>/'); % Convert pathsep's
    fprintf(fid_xml, '<matlabPath>/%s/</matlabPath>', expected_folders_other);
    fprintf(fid_xml, '<matlabPath>/</matlabPath>');
    fprintf(fid_xml, '</matlabPaths>');
    fprintf(fid_xml, '</configuration>');
    clear ocu_xml;

    file_xml = 'WITio_mltbx/metadata/coreProperties.xml';
    fid_xml = fopen(file_xml, 'w', 'n', 'UTF-8');
    if fid_xml == -1, error('Cannot open ''%s'' for writing.', file_xml); end
    ocu_xml = onCleanup(@() fclose(fid_xml)); % Ensure file stream closes on error
    fprintf(fid_xml, '<?xml version="1.0" encoding="UTF-8" standalone="yes" ?>');
    fprintf(fid_xml, '<cp:coreProperties xmlns:cp="http://schemas.openxmlformats.org/package/2006/metadata/core-properties" xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:dcmitype="http://purl.org/dc/dcmitype/" xmlns:dcterms="http://purl.org/dc/terms/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">');
    fprintf(fid_xml, '<dcterms:created xsi:type="dcterms:W3CDTF">%s</dcterms:created>', datestrnow);
    fprintf(fid_xml, '<dc:creator>%s</dc:creator>', WITio_authnamewatermark);
    fprintf(fid_xml, '<dc:description>%s</dc:description>', WITio_summary);
    fprintf(fid_xml, '<dcterms:modified xsi:type="dcterms:W3CDTF">%s</dcterms:modified>', datestrnow);
    fprintf(fid_xml, '<dc:title>%s</dc:title>', WITio_appname);
    fprintf(fid_xml, '<cp:version>%s</cp:version>', WITio_version);
    fprintf(fid_xml, '</cp:coreProperties>');
    clear ocu_xml;

    file_xml = 'WITio_mltbx/metadata/examples.xml';
    fid_xml = fopen(file_xml, 'w', 'n', 'UTF-8');
    if fid_xml == -1, error('Cannot open ''%s'' for writing.', file_xml); end
    ocu_xml = onCleanup(@() fclose(fid_xml)); % Ensure file stream closes on error
    fprintf(fid_xml, '<?xml version="1.0" encoding="UTF-8" standalone="yes" ?>');
    fprintf(fid_xml, '<examples xmlns="http://schemas.mathworks.com/matlab/addon/2014/examples"/>');
    clear ocu_xml;

    file_xml = 'WITio_mltbx/metadata/filesystemManifest.xml';
    fid_xml = fopen(file_xml, 'w', 'n', 'UTF-8');
    if fid_xml == -1, error('Cannot open ''%s'' for writing.', file_xml); end
    ocu_xml = onCleanup(@() fclose(fid_xml)); % Ensure file stream closes on error
    fprintf(fid_xml, '<?xml version="1.0" encoding="UTF-8" standalone="yes" ?>');
    fprintf(fid_xml, '<fileEntries xmlns="http://schemas.mathworks.com/package/2013/filesystemManifest" createdByEncoding="windows-1252" createdByPlatform="win64" version="2.0">');
    for ii = 1:numel(files),
        file_ii = strrep(files{ii}, 'WITio_mltbx', '');
        file_ii = strrep(file_ii, filesep, '/'); % Convert filesep's to /'s
        content = file_ii;
        date = datestrs{ii};
        name = decode_unsafe_chars(strrep(file_ii, '/fsroot', ''));
        permissions = '0666';
        if strcmp(exts{ii}, '.bat'),
            permissions = '0777';
        end
        fprintf(fid_xml, '<fileEntry content="%s" date="%s" name="%s" permissions="%s" type="File"/>', content, date, name, permissions);
    end
    fprintf(fid_xml, '</fileEntries>');
    clear ocu_xml;

    file_xml = 'WITio_mltbx/metadata/mwcoreProperties.xml';
    fid_xml = fopen(file_xml, 'w', 'n', 'UTF-8');
    if fid_xml == -1, error('Cannot open ''%s'' for writing.', file_xml); end
    ocu_xml = onCleanup(@() fclose(fid_xml)); % Ensure file stream closes on error
    fprintf(fid_xml, '<?xml version="1.0" encoding="UTF-8" standalone="yes" ?>');
    fprintf(fid_xml, '<mwcoreProperties xmlns="http://schemas.mathworks.com/package/2012/coreProperties">');
    fprintf(fid_xml, '<contentType>application/vnd.mathworks.matlab.toolbox</contentType>');
    fprintf(fid_xml, '<contentTypeFriendlyName>MATLAB Toolbox</contentTypeFriendlyName>');
    fprintf(fid_xml, '<matlabRelease>R2014b</matlabRelease>');
    fprintf(fid_xml, '</mwcoreProperties>');
    clear ocu_xml;

    file_xml = 'WITio_mltbx/metadata/systemRequirements.xml';
    fid_xml = fopen(file_xml, 'w', 'n', 'UTF-8');
    if fid_xml == -1, error('Cannot open ''%s'' for writing.', file_xml); end
    ocu_xml = onCleanup(@() fclose(fid_xml)); % Ensure file stream closes on error
    fprintf(fid_xml, '<?xml version="1.0" encoding="UTF-8" standalone="yes" ?>');
    fprintf(fid_xml, '<systemRequirements xmlns="http://schemas.mathworks.com/matlab/addon/2014/systemRequirements">');
    fprintf(fid_xml, '<requiredProducts><product identifier="1" name="MATLAB" version="8.4"/></requiredProducts>');
    fprintf(fid_xml, '</systemRequirements>');
    clear ocu_xml;

    file_xml = 'WITio_mltbx/[Content_Types].xml';
    fid_xml = fopen(file_xml, 'w', 'n', 'UTF-8');
    if fid_xml == -1, error('Cannot open ''%s'' for writing.', file_xml); end
    ocu_xml = onCleanup(@() fclose(fid_xml)); % Ensure file stream closes on error
    fprintf(fid_xml, '<?xml version="1.0" encoding="UTF-8" standalone="yes" ?>');
    fprintf(fid_xml, '<Types xmlns="http://schemas.openxmlformats.org/package/2006/content-types">');
    exts_unique = unique([exts; {'.rels'; '.xml'}]);
    for ii = 1:numel(exts_unique),
        exts_unique_ii = exts_unique{ii};
        if strcmp(exts_unique_ii, '.html'),
            content_type = 'text/html';
        elseif strcmp(exts_unique_ii, '.m'),
            content_type = 'text/plain;charset=utf-8';
        elseif strcmp(exts_unique_ii, '.txt'),
            content_type = 'text/plain;charset=utf-8';
        elseif strcmp(exts_unique_ii, '.png'),
            content_type = 'image/png';
        elseif strcmp(exts_unique_ii, '.rels'),
            content_type = 'application/vnd.openxmlformats-package.relationships+xml';
        elseif strcmp(exts_unique_ii, '.svg'),
            content_type = 'image/svg+xml';
        elseif strcmp(exts_unique_ii, '.xml'),
            content_type = 'application/vnd.mathworks.matlab.addon.properties+xml';
        elseif strcmp(exts_unique_ii, ''),
            continue;
        else,
            content_type = 'application/vnd.mathworks.matlab.filesystemDefault';
        end
        extension = exts_unique_ii(2:end); % Without dot
        fprintf(fid_xml, '<Default ContentType="%s" Extension="%s"/>', content_type, extension);
    end
    for ii = 1:numel(files),
        if strcmp(exts{ii}, ''),
            file_ii = strrep(files{ii}, 'WITio_mltbx', '');
            part_name = strrep(file_ii, filesep, '/'); % Convert filesep's to /'s
            fprintf(fid_xml, '<Override ContentType="application/vnd.mathworks.matlab.filesystemDefault" PartName="%s"/>', part_name);
        end
    end
    fprintf(fid_xml, '<Override ContentType="application/vnd.mathworks.matlab.addon.applications+xml" PartName="/metadata/applications.xml"/>');
    fprintf(fid_xml, '<Override ContentType="application/vnd.mathworks.matlab.addon.configuration+xml" PartName="/metadata/configuration.xml"/>');
    fprintf(fid_xml, '<Override ContentType="application/vnd.openxmlformats-package.core-properties+xml" PartName="/metadata/coreProperties.xml"/>');
    fprintf(fid_xml, '<Override ContentType="application/vnd.mathworks.matlab.addon.examples+xml" PartName="/metadata/examples.xml"/>');
    fprintf(fid_xml, '<Override ContentType="application/vnd.mathworks.package.filesystemManifest+xml" PartName="/metadata/filesystemManifest.xml"/>');
    fprintf(fid_xml, '<Override ContentType="application/vnd.mathworks.package.coreProperties+xml" PartName="/metadata/mwcoreProperties.xml"/>');
    fprintf(fid_xml, '<Override ContentType="application/vnd.mathworks.matlab.addon.systemRequirements+xml" PartName="/metadata/systemRequirements.xml"/>');
    fprintf(fid_xml, '</Types>');
    clear ocu_xml;

    % Copy PNG file
    copyfile(WITio_screenshot, 'WITio_mltbx/metadata/primaryScreenShot.png')

    % Finalize MLTBX file
    if exist('WITio.zip', 'file') == 2,
        delete('WITio.zip');
    end
    zip('WITio.zip', '*', 'WITio_mltbx');
    if exist('WITio.mltbx', 'file') == 2,
        delete('WITio.mltbx');
    end
    movefile('WITio.zip', 'WITio.mltbx');
    rmdir('WITio_mltbx', 's');

    function strs = encode_unsafe_chars(strs),
        % Encode some unsafe ASCII characters with URL safe HTML encoding
        unsafe_chars = ['%', ' !#$&()[]{}']; % Percentage must be processed first!
        for jj = 1:numel(unsafe_chars),
            strs = strrep(strs, unsafe_chars(jj), sprintf('%%%02X', uint8(unsafe_chars(jj))));
        end
    end

    function strs = decode_unsafe_chars(strs),
        % Decode some unsafe ASCII characters from URL safe HTML encoding
        unsafe_chars = [' !#$&()[]{}', '%']; % Percentage must be processed last!
        for jj = 1:numel(unsafe_chars),
            strs = strrep(strs, sprintf('%%%02X', uint8(unsafe_chars(jj))), unsafe_chars(jj));
        end
    end

    function encode_unsafe_chars_dir_files_recursively(folder),
        S = dir(folder);
        for jj = 1:numel(S),
            if ~strcmp(S(jj).name, '.') && ~strcmp(S(jj).name, '..'), % Skip . and ..
                name_ii = S(jj).name;
                name_ii_no_specials = encode_unsafe_chars(name_ii);
                if ~strcmp(name_ii, name_ii_no_specials),
                    movefile(fullfile(folder, name_ii), fullfile(folder, name_ii_no_specials));
                end
                if S(jj).isdir,
                    encode_unsafe_chars_dir_files_recursively(fullfile(folder, S(jj).name));
                end
            end
        end
    end
end
