% MIT No Attribution License (https://opensource.org/license/mit-0)
% 
% Copyright (c) 2019 Joonas T. Holmi (jtholmi@gmail.com), 2019-2021 Aalto University

% This tests WITio.obj.wid, WITio.obj.wip, and WITio.obj.wit syncing.
function wid_wip_wit_sync(debug),
    file_ref = fullfile(WITio.tbx.path.demo, 'A_v5.wip');
    N_O_wid_known = 31; % Count all Data both plottable and non-plottable

    % Test ref. file Tree, Project and Data consistency
    O_wip_all_before_read = WITio.obj.wip.Projects.all;
    [O_wid, O_wip, O_wit] = WITio.read(file_ref, '-all', '-Manager', '--all');
    O_wip_all_after_read = WITio.obj.wip.Projects.all;
    if numel(O_wit) ~= 1 || numel(O_wip) ~= 1 || isempty(O_wid) || ...
            numel(O_wid) ~= N_O_wid_known || ...
            numel(O_wid) ~= numel(O_wip.Data) || ...
            2*numel(O_wid)+1 ~= numel(O_wip.TreeData.Children),
        error('Expected a Tree, a Project and %d of Data (self-consistent)!', N_O_wid_known);
    end

    % Test syncronous link between Project and Tree
    O_wip_sync = WITio.obj.wip.Projects.match_any(@(O_wip) O_wip.Tree == O_wit);
    if numel(O_wip_all_before_read)+1 ~= numel(O_wip_all_after_read) || ...
            numel(O_wip_sync) ~= numel(O_wip) || O_wip_sync ~= O_wip,
        error('Expected link to Project for given Tree!');
    end

    % Test syncronous link on a Data copy
    OrdinalNumber = O_wid(end).OrdinalNumber;
    DataClassName_Data = O_wid(end).TreeDataPair;
    O_wid_end_copy = O_wid(end).copy();
    OrdinalNumber_copy = O_wid_end_copy.OrdinalNumber;
    DataClassName_Data_copy = O_wid_end_copy.TreeDataPair;
    if O_wip.Data(OrdinalNumber_copy+1) ~= O_wid_end_copy || ...
            OrdinalNumber == OrdinalNumber_copy || ...
            any(DataClassName_Data == DataClassName_Data_copy),
        error('Expected Tree and Project Data updates at a Data copy!');
    end

    % Test syncronous link on a Data deletion
    N_O_wip_Data_isvalid_before_Data_deletion = numel([O_wip.Data]);
    delete(O_wid_end_copy);
    N_O_wip_Data_isvalid_after_Data_deletion = numel([O_wip.Data]);
    if N_O_wip_Data_isvalid_before_Data_deletion ~= N_O_wip_Data_isvalid_after_Data_deletion+1 || ...
            all(DataClassName_Data.isvalid) ~= true || ...
            all(DataClassName_Data_copy.isvalid) ~= false,
        error('Expected Tree and Project Data updates at a Data deletion!');
    end

    % Test syncronous link on Tree deletion
    O_wip_all_before_Tree_deletion = WITio.obj.wip.Projects.all;
    O_wip_isvalid_before_Tree_deletion = O_wip.isvalid;
    O_wid_isvalid_before_Tree_deletion = O_wid.isvalid;
    delete(O_wit);
    O_wip_isvalid_after_Tree_deletion = O_wip.isvalid;
    O_wid_isvalid_after_Tree_deletion = O_wid.isvalid;
    O_wip_all_after_Tree_deletion = WITio.obj.wip.Projects.all;
    if numel(O_wip_all_before_Tree_deletion) ~= numel(O_wip_all_after_Tree_deletion)+1 || ...
            O_wip_isvalid_before_Tree_deletion ~= true || ...
            all(O_wid_isvalid_before_Tree_deletion) ~= true || ...
            O_wip_isvalid_after_Tree_deletion ~= false || ...
            all(O_wid_isvalid_after_Tree_deletion) ~= false,
        error('Expected Project deletion and Data deletion at Tree deletion!');
    end

    % Test syncronous link on Project deletion
    [O_wid, O_wip, O_wit] = WITio.read(file_ref, '-all', '-Manager', '--all');
    O_wip_all_before_Project_deletion = WITio.obj.wip.Projects.all;
    O_wit_isvalid_before_Project_deletion = O_wit.isvalid;
    O_wid_isvalid_before_Project_deletion = O_wid.isvalid;
    delete(O_wip);
    O_wit_isvalid_after_Project_deletion = O_wit.isvalid;
    O_wid_isvalid_after_Project_deletion = O_wid.isvalid;
    O_wip_all_after_Project_deletion = WITio.obj.wip.Projects.all;
    if numel(O_wip_all_before_Project_deletion) ~= numel(O_wip_all_after_Project_deletion)+1 || ...
            O_wit_isvalid_before_Project_deletion ~= true || ...
            all(O_wid_isvalid_before_Project_deletion) ~= true || ...
            O_wit_isvalid_after_Project_deletion ~= false || ...
            all(O_wid_isvalid_after_Project_deletion) ~= false,
        error('Expected Tree deletion and Data deletion at Project deletion!');
    end

    % Test syncronous link on all Data deletion
    [O_wid, O_wip, O_wit] = WITio.read(file_ref, '-all', '-Manager', '--all');
    O_wip_all_before_Data_deletion = WITio.obj.wip.Projects.all;
    O_wit_isvalid_before_Data_deletion = O_wit.isvalid;
    O_wip_isvalid_before_Data_deletion = O_wip.isvalid;
    N_O_wip_Data_isvalid_before_Data_deletion = numel([O_wip.Data]);
    delete(O_wid);
    O_wit_isvalid_after_Data_deletion = O_wit.isvalid;
    O_wip_isvalid_after_Data_deletion = O_wip.isvalid;
    N_O_wip_Data_isvalid_after_Data_deletion = numel([O_wip.Data]);
    O_wip_all_after_Data_deletion = WITio.obj.wip.Projects.all;
    if numel(O_wip_all_before_Data_deletion) ~= numel(O_wip_all_after_Data_deletion) || ...
            O_wit_isvalid_before_Data_deletion ~= true || ...
            O_wip_isvalid_before_Data_deletion ~= true || ...
            N_O_wip_Data_isvalid_before_Data_deletion ~= N_O_wid_known || ...
            O_wit_isvalid_after_Data_deletion ~= true || ...
            O_wip_isvalid_after_Data_deletion ~= true || ...
            N_O_wip_Data_isvalid_after_Data_deletion ~= 0,
        error('Expected Project Data deletion at Data deletion!');
    end

    % Test no syncronous link when only Tree is read from file
    O_wit = WITio.obj.wit.read(file_ref);
    O_wip_sync = WITio.obj.wip.Projects.match_any(@(O_wip) O_wip.Tree == O_wit); % Due to syncronous link between Project and its Tree, Project's Tree is always Root AND never invalid/deleted!
    if ~isempty(O_wip_sync),
        error('Expected no link to Project for given Tree!');
    end

    % Test syncronous link on a Project creation
    O_wip = WITio.obj.wip(O_wit);
    O_wip_sync = WITio.obj.wip.Projects.match_any(@(O_wip) O_wip.Tree == O_wit); % Due to syncronous link between Project and its Tree, Project's Tree is always Root AND never invalid/deleted!
    if numel(O_wip_sync) ~= numel(O_wip) || O_wip_sync ~= O_wip,
        error('Expected link to Project for given Tree on a Project creation!');
    end

    % Test syncronous link when creating customized Tree, Project, and Data
    O_wit = WITio.obj.wip.new(); % Create new Project (*.WIP-format) WIT-tree root
    O_wip = WITio.obj.wip(O_wit); % Create its Project and Data objects
    % Create many simple TDText objects to test for the destructor
    for ii = 1:10,
        % Test syncronous link (via Project Data) prevent a Data deletion
        new_TDText = WITio.obj.wid.new_Text(O_wit);
        % For ii > 1: Above clears a handle reference to the previous
        % TDText object. Nothing happens when Project automatically stores
        % references to all Data handles and prevents multiple Data handles
        % to same Tree branches. If no references remain, then the previous
        % TDText object is incorrectly deleted.
        new_TDText.Name = sprintf('TDText %d', ii); % Add unique name
        if numel(O_wip.Data) ~= ii || ...
                2*ii+1 ~= numel(O_wip.TreeData.Children),
            error('Expected no Data deletion when creating Data!');
        end
    end
end
